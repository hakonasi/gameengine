#include "entityManager.h"

EntityManager::EntityManager(){
//    entities.reserve(100);
}

EntityManager::~EntityManager(){

}

std::vector<Entity>& EntityManager::GetEntities(){
    return entities;
}

void EntityManager::AddEntity(Entity entity){
    entities.push_back(entity);
}

void EntityManager::Reset(){
    entities.clear();
    Entity::s_indicesCounter = 0;
}

void EntityManager::DeleteEntity(Entity* entity){
    size_t j = 0;
    for(auto& i : entities){
        if(&i == entity){
            entities.erase(entities.begin() + j);
            return;
        }
        else{
            j++;
        }
    }
    std::cout << "[WARNING] Entity was not deleted " << entity << " [" << entity->GetName() << "]" << std::endl;
}
