#include "camera.h"

Camera::Camera(){

}

Camera::Camera(glm::vec3 position, float fov, float near, float far, Time* m_time){
    this->position = position;
    this->fov = fov;
    this->near = near;
    this->far = far;
    this->m_time = m_time;

    target = glm::vec3(0.0f, 0.0f, 0.0f);
    direction = glm::normalize(position - target);
    right = glm::normalize(glm::cross(up, direction));
    front = glm::vec3(0.0f, 0.0f, -1.0f);
    projection = glm::perspective(glm::radians(fov), 4.0f/3.0f, near, far);
    yaw = pitch = 0.0f;
    glm::mat4 view = glm::lookAt(
        position,
        glm::vec3(0,0,0),
        glm::vec3(0,1,0)
    );
}

Camera::~Camera(){

}

void Camera::UpdateProjection(int width, int height){
    projection = glm::perspective(glm::radians(fov), (float)width/(float)height, near, far);
}

void Camera::Movement(){
    view = glm::lookAt(position, position + front, up);
}

glm::mat4 Camera::GetView(){
    return view;
}

glm::mat4 Camera::GetProjection(){
    return projection;
    printf("%.3f %.3f %.3f\n", position.x, position.y, position.z);
}

void Camera::Front(){
    position += front * speed * m_time->GetDeltaTime();
}

void Camera::Back(){
    position -= front * speed * m_time->GetDeltaTime();
}

void Camera::Left(){
    position -= glm::normalize(glm::cross(front, up)) * speed * m_time->GetDeltaTime();
}

void Camera::Right(){
    position += glm::normalize(glm::cross(front, up)) * speed * m_time->GetDeltaTime();
}

void Camera::Up(){
    position += glm::vec3(0.0f, 1.0f, 0.0f) * speed * m_time->GetDeltaTime();
}

void Camera::Down(){
    position -= glm::vec3(0.0f, 1.0f, 0.0f) * speed * m_time->GetDeltaTime();
}

void Camera::SetSpeed(float speed){
    this->speed = speed;
}

glm::vec3 Camera::GetPosition(){
    return position;
}

void Camera::Yaw(float offset){
    yaw += offset * m_time->GetDeltaTime();
    if(yaw > 360)
        yaw-=360;
    if(yaw < -360)
        yaw+=360;

    glm::vec3 m_front;
    m_front.x = cos(glm::radians(pitch)) * cos(glm::radians(yaw));
    m_front.y = sin(glm::radians(pitch));
    m_front.z = cos(glm::radians(pitch)) * sin(glm::radians(yaw));
    front = glm::normalize(m_front);
}

void Camera::Pitch(float offset){
    pitch += offset * m_time->GetDeltaTime();
    if(pitch > 89.0f)
        pitch = 89.0f;
    if(pitch < -89.0f)
        pitch = -89.0f;

    glm::vec3 m_front;
    m_front.x = cos(glm::radians(pitch)) * cos(glm::radians(yaw));
    m_front.y = sin(glm::radians(pitch));
    m_front.z = cos(glm::radians(pitch)) * sin(glm::radians(yaw));
    front = glm::normalize(m_front);
}
