#version 330 core

in vec2 uv;
in vec3 normal;
in vec3 fragPos;
in vec3 cameraPos;

out vec3 color;

struct Light{
    vec3 position;
    vec3 color;

    float range;
    float intensity;
};

struct Material{
    vec3 albedo_col;
    sampler2D albedo_tex;

    float metallic;
    float roughness;
    float ao;
};

uniform Material material;
uniform mat4 modelMatrix;
uniform Light light;

const float PI = 3.1415f;

vec3 fresnelSchlick(float cosTheta, vec3 F0){
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a      = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float num   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return num / denom;
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float num   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return num / denom;
}
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}

vec3 calcPointLight(vec3 norm, vec3 F0){
    vec3 N = normalize(norm);
    vec3 V = normalize(cameraPos - fragPos);
    vec3 L = normalize(light.position - fragPos); // для точечного света (point light)
    vec3 H = normalize(V + L);

    float distance      = length(light.position - fragPos) / (light.range + 0.001);
    float attenuation   = 1.0 / (distance * distance);
    vec3 radiance       = light.color * attenuation;

    vec3 F      = fresnelSchlick(max(dot(H, V), 0.0), F0);
    float NDF   = DistributionGGX(N, H, material.roughness);
    float G     = GeometrySmith(N, V, L, material.roughness);

    vec3 numerator      = NDF * G * F;
    float denominator   = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.001;
    vec3 specular       = numerator / denominator;

    vec3 kS = F;
    vec3 kD = vec3(1.0) - kS;

    kD *= 1.0 - material.metallic;

    float NdotL = max(dot(N, L), 0.0);
    return (kD * material.albedo_col / PI + specular) * radiance * NdotL;
}

vec3 calcDirectionalLight(vec3 norm, vec3 F0){
    vec3 N = normalize(norm);
    vec3 V = normalize(cameraPos - fragPos);
    vec3 L = normalize(light.position);
    vec3 H = normalize(V + L);

    vec3 radiance       = light.color;

    vec3 F      = fresnelSchlick(max(dot(H, V), 0.0), F0);
    float NDF   = DistributionGGX(N, H, material.roughness);
    float G     = GeometrySmith(N, V, L, material.roughness);

    vec3 numerator      = NDF * G * F;
    float denominator   = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.001;
    vec3 specular       = numerator / denominator;

    vec3 kS = F;
    vec3 kD = vec3(1.0) - kS;

    kD *= 1.0 - material.metallic;

    float NdotL = max(dot(N, L), 0.0);
    return (kD * material.albedo_col / PI + specular) * radiance * NdotL;
}
void main(){
    vec3 norm = mat3(transpose(inverse(modelMatrix))) * normal;

    vec3 F0 = vec3(0.04);
    F0 = mix(F0, material.albedo_col, material.metallic);
    vec3 Lo;
    
    if(light.range < 0){
        Lo = calcDirectionalLight(norm, F0) * light.intensity;
    }
    else{
        Lo = calcPointLight(norm, F0) * light.intensity;
    }
    
    vec3 ambient = vec3(0.03) * material.albedo_col * material.ao;
    //color = texture(material.albedo_tex, uv).rgb *material.albedo_col;
    color = ambient + Lo * texture(material.albedo_tex, uv).rgb;
    color = color / (color + vec3(1.0));
    color = pow(color, vec3(1.0/2.2));
}
