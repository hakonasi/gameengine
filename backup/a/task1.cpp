#include <iostream>

char big[] = {
'A', 'B', 'C', 'D', 'E', 'F', 'G',
'H', 'I', 'J', 'K', 'L', 'M', 'N',
'O', 'P', 'Q', 'R', 'S', 'T', 'U',
'V', 'W', 'X', 'Y', 'Z'
};

char little[] = {
'a', 'b', 'c', 'd', 'e', 'f', 'g',
'h', 'i', 'j', 'k', 'l', 'm', 'n',
'o', 'p', 'q', 'r', 's', 't', 'u',
'v', 'w', 'x', 'y', 'z'
};

int main(){
    char a;
    std::cin >> a;
    int i = 0;
    for(; i < 26; i++){
        if(big[i] == a){
            std::cout << big[i-1] << " " << big[i+1];
        }
        if(little[i] == a){
            std::cout << little[i-1] << " " << little[i+1];
        }
    }
    return 0;
}
