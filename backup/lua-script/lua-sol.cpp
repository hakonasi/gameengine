#include <iostream>
#include <sol/sol.hpp>

int main(){
    sol::state luaState;
    luaState.open_libraries(sol::lib::base);

    luaState["a"] = 10;
    luaState.script_file("script.lua");
    return 0;
} 
