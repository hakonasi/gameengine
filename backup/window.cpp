#include "window.h"

// устанавливаем параметры окна и версию opengl
Window::Window(int width, int height, const char* title, uint glMajor, uint glMinor){
    this->m_width = width;
    this->m_height = height;

    this->m_glMajor = glMajor;
    this->m_glMinor = glMinor;

    this->title = title;
}

Window::~Window(){
    glfwTerminate();
}

GLFWwindow* Window::GetWindow(){
    return m_window;
}

bool Window::Init(){
    if(!glfwInit()){
        printf("Failed to init GLFW.\n");
        return false;
    }

    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, m_glMajor);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, m_glMinor);
    
    m_window = glfwCreateWindow(m_width, m_height, title, NULL, NULL);
    if(!m_window){
        printf("Failed to create window.\n");
        glfwTerminate();
        return false;
    }

    glfwMakeContextCurrent(m_window);

    glewExperimental=GL_TRUE;
    if(glewInit() != GLEW_OK){
        printf("Failed to init GLEW.\n");
        glfwTerminate();
        return false;
    }

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
//    glfwDisable(GLFW_MOUSE_CURSOR);

    glfwSetWindowUserPointer(m_window, this);  
    glfwSetWindowSizeCallback(m_window, ResizeCallback);    
    return true;
}

// NS = non static
void Window::ResizeCallbackNS(int width, int height){
    printf("New window size %d %d\n", width, height);
    glViewport(0, 0, width, height);
    camera->UpdateProjection(width, height); 
    this->m_width = width;
    this->m_height = height;
}

void Window::ResizeCallback(GLFWwindow* window, int width, int height){
    Window* obj = static_cast<Window*>(glfwGetWindowUserPointer(window));
    obj->ResizeCallbackNS(width, height); 
}

void Window::SetSize(int width, int height){
    this->m_width = width;
    this->m_height = height;
}

void Window::Exit(){
    glfwDestroyWindow(m_window);
    glfwTerminate();
}

void Window::SetInput(Input* input){
    this->m_input = input;
}

void Window::SetCamera(Camera* camera){
    this->camera = camera;
}

Input* Window::GetInput(){
    return m_input;
}

int Window::GetWidth(){
    return m_width;
}

int Window::GetHeight(){
    return m_height;
}

// передаем функцию рендера
void Window::Update( void (*rendering)()){
    while(!glfwWindowShouldClose(m_window)){
        rendering();

        glfwSwapBuffers(m_window);
        glfwPollEvents();
    }
}
