#ifndef ENTITY_H
#define ENTITY_H

#include "mesh.h"
#include "image.h"
#include "material.h"

class Entity{
    public:
        Entity();
        Entity(Mesh* mesh);
        ~Entity();
        
        void SetMesh(Mesh* mesh);
        Mesh* GetMesh();

        void SetMaterial(Material material);
        Material& GetMaterial();        

        void SetTexture(Image* image);
        void SetPosition(glm::vec3 position);
        void Render(Shader& shader, Camera* camera);
    private:
        Mesh* mesh;
        Image* image;
        glm::vec3 position;
        glm::mat4 model;

        Material material;
};

#endif
