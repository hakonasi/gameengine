#ifndef MESH_H
#define MESH_H

#include "shader.h"
#include <vector>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

class Mesh{
public:
    Mesh();
    Mesh(std::vector<glm::vec3>& vertices, std::vector<uint>& indices, std::vector<glm::vec2>& uvs);
    ~Mesh();

    void Render();
    bool ImportMesh(const char* filename);
    std::vector<Mesh>& GetChildMeshes();
private:
    void Init(std::vector<glm::vec3>& vertices, std::vector<uint>& indices, std::vector<glm::vec2>& uvs);
    void RecursiveProcess(aiNode* node, const aiScene* scene, bool& first);
    void ProcessMesh(aiMesh* mesh, const aiScene* scene, bool& first);

    uint vao;
    uint buffers[2];
    uint idx_buf;

    uint indices_size;
    
    Mesh* lastMesh;
    uint numChilds;
    std::vector<Mesh> childMeshes;
};

#endif
