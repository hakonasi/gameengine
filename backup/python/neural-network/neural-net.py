import numpy as np
import scipy.special

from PIL import Image, ImageDraw
from matplotlib import cm 

class NeuralNetwork:
    def __init__(self, inputNodes, hiddenNodes1, hiddenNodes2, outputNodes, learningRate):
        self.inodes = inputNodes
        self.hnodes1 = hiddenNodes1
        self.hnodes2 = hiddenNodes2
        self.onodes = outputNodes

        self.lr = learningRate

        self.wih1 = (np.random.rand(self.hnodes1, self.inodes) - 0.5)
        self.wh1h2 = (np.random.rand(self.hnodes2, self.hnodes1) - 0.5)
        self.wh2o = (np.random.rand(self.onodes, self.hnodes2) - 0.5)      
        
        # использование сигмоиды в качестве функции активации
        self.activation_function = lambda x: scipy.special.expit(x)
        pass

    # тренировка нейронной сети
    def train(self, inputs_list, targets_list):
        # преобразовать список входных значений в двухмерный массив
        inputs = np.array(inputs_list, ndmin=2).T
        targets = np.array(targets_list, ndmin=2).T

        # рассчитать входящие сигналы дял скрытого слоя
        hidden_inputs1 = np.dot(self.wih1, inputs)
        # рассчитать исходящие сигналы для скрытого слоя
        hidden_outputs1 = self.activation_function(hidden_inputs1)

        hidden_inputs2 = np.dot(self.wh1h2, hidden_outputs1)
        hidden_outputs2 = self.activation_function(hidden_inputs2)

        # рассчитать входящие сигналы для выходного слоя
        final_inputs = np.dot(self.wh2o, hidden_outputs2)
        # рассчитать исходящие сигналы для выходного слоя
        final_outputs = self.activation_function(final_inputs)

        # ошибка = целевое значение - фактическое значение
        output_errors = targets - final_outputs

        hidden_errors2 = np.dot(self.wh2o.T, output_errors)
        hidden_errors1 = np.dot(self.wh1h2.T, hidden_errors2)

        # обновить весовые коэффициенты связей между вторым скрытым и выходным слоями
        self.wh2o += self.lr * np.dot((output_errors * final_outputs * (1.0 - final_outputs)), np.transpose(hidden_outputs2))

        self.wh1h2 += self.lr * np.dot((hidden_errors2 * hidden_outputs2 * (1.0 - hidden_outputs2)), np.transpose(hidden_outputs1))
        
        # обновить весовые коэффициенты связей между входным и скрытым слоями
        self.wih1 += self.lr * np.dot((hidden_errors1 * hidden_outputs1 * (1.0 - hidden_outputs1)), np.transpose(inputs))
        pass

    def query(self, inputs_list):
        # преобразовать список входящих значений
        # в двумерный массив
        inputs = np.array(inputs_list, ndmin=2).T
        
        # рассчитать входящие сигналы дял скрытого слоя
        hidden_inputs1 = np.dot(self.wih1, inputs)
        # рассчитать исходящие сигналы для скрытого слоя
        hidden_outputs1 = self.activation_function(hidden_inputs1)
        
        hidden_inputs2 = np.dot(self.wh1h2, hidden_outputs1)
        hidden_outputs2 = self.activation_function(hidden_inputs2)

        # рассчитать входящие сигналы для выходного слоя
        final_inputs = np.dot(self.wh2o, hidden_outputs2)
        # рассчитать исходящие сигналы для выходного слоя
        final_outputs = self.activation_function(final_inputs)
        
        return final_outputs

# количество входных, скрытых и выходных узлов
input_nodes = 3
hidden_nodes_1 = 5
hidden_nodes_2 = 5
output_nodes = 3

# коэффициент обучения равен 0,3
learning_rate = 0.7

n = NeuralNetwork(input_nodes, hidden_nodes_1, hidden_nodes_2, output_nodes, learning_rate)

image1 = Image.open("/home/gordeyb/input-test.jpg")
width = image1.size[0]
height = image1.size[1]
training_inputs = np.asarray(image1)

image2 = Image.open("/home/gordeyb/out-test.jpg")
training_outputs = np.asarray(image2)

output_array = np.zeros((height, width, 3))

for i in range(10):
    for h in range(height):
        print("Обучение: " + str(i) + " " + str(h))
        for w in range(width):
            inputPixel = training_inputs[h][w] / 255.0
            outputPixel = training_outputs[h][w] / 255.0

            n.train(inputPixel, outputPixel)

for h in range(height):
    for w in range(width):
        inputPixel = training_inputs[h][w] / 255.0

        output_array[h][w] = n.query(inputPixel).T * 255.0

print(height)
print(width)

im = Image.fromarray(training_inputs, "RGB")
im.show()
im2 = Image.fromarray(training_outputs, "RGB")
im2.show()
im3 = Image.fromarray(np.int8(output_array), "RGB")
im3.show()
