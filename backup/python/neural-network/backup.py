import numpy as np
import scipy.special

from PIL import Image, ImageDraw
from matplotlib import cm 

class NeuralNetwork:
    def __init__(self, inputNodes, hiddenNodes, outputNodes, learningRate):
        self.inodes = inputNodes
        self.hnodes = hiddenNodes
        self.onodes = outputNodes

        self.lr = learningRate

        self.wih = (np.random.rand(self.hnodes, self.inodes) - 0.5)
        self.who = (np.random.rand(self.onodes, self.hnodes) - 0.5)      
        
        # использование сигмоиды в качестве функции активации
        self.activation_function = lambda x: scipy.special.expit(x)
        pass

    # тренировка нейронной сети
    def train(self, inputs_list, targets_list):
        # преобразовать список входных значений в двухмерный массив
        inputs = np.array(inputs_list, ndmin=2).T
        targets = np.array(targets_list, ndmin=2).T

        # рассчитать входящие сигналы дял скрытого слоя
        hidden_inputs = np.dot(self.wih, inputs)
        # рассчитать исходящие сигналы для скрытого слоя
        hidden_outputs = self.activation_function(hidden_inputs)

        # рассчитать входящие сигналы для выходного слоя
        final_inputs = np.dot(self.who, hidden_outputs)
        # рассчитать исходящие сигналы для выходного слоя
        final_outputs = self.activation_function(final_inputs)

        # ошибка = целевое значение - фактическое значение
        output_errors = targets - final_outputs

        hidden_errors = np.dot(self.who.T, output_errors)

        # обновить весовые коэффициенты связей между скрытым и выходным слоями
        self.who += self.lr * np.dot((output_errors * final_outputs * (1.0 - final_outputs)), np.transpose(hidden_outputs))

        # обновить весовые коэффициенты связей между входным и скрытым слоями
        self.wih += self.lr * np.dot((hidden_errors * hidden_outputs * (1.0 - hidden_outputs)), np.transpose(inputs))
        pass

    def query(self, inputs_list):
        # преобразовать список входящих значений
        # в двумерный массив
        inputs = np.array(inputs_list, ndmin=2).T
        
        # рассчитать входящие сигналы дял скрытого слоя
        hidden_inputs = np.dot(self.wih, inputs)
        # рассчитать исходящие сигналы для скрытого слоя
        hidden_outputs = self.activation_function(hidden_inputs)
        
        # рассчитать входящие сигналы для выходного слоя
        final_inputs = np.dot(self.who, hidden_outputs)
        # рассчитать исходящие сигналы для выходного слоя
        final_outputs = self.activation_function(final_inputs)
        
        return final_outputs

np.random.rand(3,3) - 0.5

# количество входных, скрытых и выходных узлов
input_nodes = 3
hidden_nodes = 10
output_nodes = 3

# коэффициент обучения равен 0,3
learning_rate = 0.3

n = NeuralNetwork(input_nodes, hidden_nodes, output_nodes, learning_rate)

image1 = Image.open("/home/gordeyb/out-test.jpg")
width = image1.size[0]
height = image1.size[1]
training_inputs = np.asarray(image1)

image2 = Image.open("/home/gordeyb/input-test.jpg")
training_outputs = np.asarray(image2)

output_array = np.zeros((height, width, 3))

for i in range(5):
    for h in range(height):
        print("Обучение: " + str(i) + " " + str(h))
        for w in range(width):
            inputPixel = training_inputs[h][w] / 255.0
            outputPixel = training_outputs[h][w] / 255.0

            n.train(inputPixel, outputPixel)

for h in range(height):
    for w in range(width):
        inputPixel = training_inputs[h][w] / 255.0

        output_array[h][w] = n.query(inputPixel).T * 255.0

print(height)
print(width)

im = Image.fromarray(training_inputs, "RGB")
im.show()
im2 = Image.fromarray(training_outputs, "RGB")
im2.show()
im3 = Image.fromarray(np.int8(output_array), "RGB")
im3.show()
