import numpy as np
from PIL import Image, ImageDraw
from matplotlib import cm 

def sigmoid(x):
    return 1/(1 + np.exp(-x))

def activation(x):
    a = sigmoid(x)
    return a * (1 - a)

image1 = Image.open("/home/gordeyb/input-test.jpg")
width = image1.size[0]
height = image1.size[1]
pixels_array = np.asarray(image1)

image2 = Image.open("/home/gordeyb/out-test.jpg")
graypixels_array = np.asarray(image2)

f = open("/home/gordeyb/w-data.txt", 'w')

'''
if(mode == 0):
    for i in range(width):
        for j in range(height):
            r = pix[i, j][0]
            g = pix[i, j][0]
            b = pix[i, j][0]
            draw.point((i, j), (r, r, r))
'''
input_neurons = np.zeros(3)
output_training = np.zeros(3)
output = np.zeros(3)
error = np.zeros(3)
hidden_neurons_1 = np.zeros(5)
hidden_neurons_2 = np.zeros(5)
hidden_neurons_error_1 = np.zeros(5)
hidden_neurons_error_2 = np.zeros(5)
hidden_weights_1 = np.random.random((5, 3)) - 0.5
hidden_weights_2 = np.random.random((5, 5)) - 0.5
hidden_weights_out = np.random.random((5, 3)) - 0.5

for i in range(1):
    #print(str(i) + "\t" + str(output_pixel) + "\t" + str(output))
    
    for j in range(height):
        #print(str(i) + " " + str(j) + "\tРезультат: " + str(output_training) + "\t" + str(output) + "\tОшибка:" + str(error))
        for k in range(width):
            # входной пиксель
            input_neurons = pixels_array[j][k] / 255            

            # что мы хотим получить
            output_training = graypixels_array[j][k] / 255

            for e in range(5):
                hidden_neurons_1[e] = np.dot(input_neurons, hidden_weights_1[e])

            for e in range(5):
                hidden_neurons_2[e] = sigmoid(np.dot(hidden_neurons_1, hidden_weights_2[e]))
            
            output = sigmoid( np.dot(hidden_neurons_2, hidden_weights_out) )
            print(str(input_neurons) + " " + str(output))
            # ошибка выходного нейрона
            error = output_training - output
            #print("j " + str(j) + " k " + str(k))
            # ошибка 2-го скрытого слоя
            for e in range(5):
                hidden_neurons_error_2[e] = np.dot(hidden_weights_out[e], error)
            # ошибка 1-го скрытого слоя
            for e in range(5):
                hidden_neurons_error_1[e] = np.dot(hidden_weights_2[e], hidden_neurons_error_2)
                
            # метод обратного распространения ошибки
            for e in range(5):
                hidden_weights_1[e] = hidden_weights_1[e] + np.dot(hidden_neurons_error_1[e] * activation(hidden_neurons_1[e]) * 0.5, input_neurons)

            for e in range(5):
                hidden_weights_2[e] = hidden_weights_2[e] + np.dot(hidden_neurons_error_2[e] * activation(hidden_neurons_2[e]) * 0.5, hidden_neurons_1)
            
            for e in range(5):    
                for b in range(3):            
                    hidden_weights_out[e][b] = hidden_weights_out[e][b] + error[b]  * activation(output[b]) * hidden_neurons_2[e] * 0.5 

f.write(str(hidden_weights_1))
f.write("\n\n")
f.write(str(hidden_weights_2))
f.write("\n\n")
f.write(str(hidden_weights_out))

'''
hidden_weights_1 = np.array([[  626.5376012,    532.65118478,  547.19969929],
 [-2660.66465889, -2392.0747309,  -2329.08546243],
 [ -561.46160932 , -514.21358843,  -527.95339666],
 [ -843.01355255,  -761.1617007 ,  -781.97005868],
 [-1416.6427419,  -1257.82010254 ,-1286.64198059],
 [ -439.75691887 , -416.42494977 , -401.99351214],
 [  476.12775548 ,  402.23420276,   391.71048307],
 [-1298.77581366, -1179.32874049, -1133.43882553],
 [-1381.06189454, -1249.60262758, -1221.854906  ],
 [-1424.35662822 ,-1289.26979403 ,-1252.82122519]])

hidden_weights_2 = np.array([[ -1.90166053 , -3.06571232,   3.07131471 , 12.0469072 ,  -1.65818236, 1.58010726,  -0.83820926 , -3.08053501 , -3.902576 ,   -1.91950156],
 [ -7.51142531,   0.59603667 , -2.24638848,  -7.8148934,    0.54324586, -2.52356212,  -7.66176008 ,  0.58124269,   0.79495563 ,  0.56317724],
 [  4.24326864,  -1.3961003 ,  -0.4278856,    0.62839966,  -0.22810954, -0.63040053 ,  4.17403755 , -0.36660708,  -0.52209425 , -0.89383087],
 [ -1.80381943 , -1.32437834 , -0.4691257,   -0.26625637 , -0.13013097, -0.39681653  ,-2.04927199 , -0.39502953,  -0.74666308,  -0.0809288 ],
 [  0.86809917  , 0.84085851,  -1.47614102, -11.0969996,    0.15777678, -1.05290459 ,  0.75248887 ,  1.49713474,  2.04308211 ,  0.78285437],
 [ -1.36515612,  -1.70942697,   0.23173004 ,  3.08757549 , -0.86280045, -0.60565225,  -2.07689725 , -0.98394881 , -1.29071801 , -1.06459187],
 [  0.31876258,  -2.40897032,   2.6830434 ,  12.06728181,  -1.41296007,  2.02849985 ,  0.11801144 , -2.68412946 , -3.64350328 , -2.39929387],
 [ -0.15521261 ,  0.80994906 , -1.60232814, -12.98570174,   0.09858161, -1.5294639 ,   0.05866033 ,  1.21849447 ,  1.03311027 ,  1.01460436],
 [  2.03259821,   0.80303587,  -2.11632317, -11.15617906,   0.0651669, -1.72114483,   2.58717901 ,  1.22370478 ,  0.6067259,    1.23163003],
 [ -0.64802068,   0.4612651 ,  -2.32810832, -11.3397902 ,   0.18207095, -1.63646376,  -1.22159008  , 1.37633008 ,  1.16514517,   0.63318199]])

hidden_weights_out = np.array([[ 0.36751871 , 0.01415658 ,-0.02529722],
 [ 0.05020139 ,-0.11099674, -0.30910087],
 [ 1.66396894 , 1.71489229,  1.72477257],
 [ 0.42749877 ,-0.12772396, -0.09927267],
 [ 0.55736206 , 1.10241873,  0.51060739],
 [ 0.33351513 , 0.06727521, -0.146285  ],
 [ 0.57723258 , 0.50394763,  0.42177035],
 [ 0.50901756 , 0.33195962,  0.39443071],
 [ 1.2482211  , 0.97964213,  1.40923874],
 [ 0.36220478,  0.15006038,  0.05574547]])
'''
output_array = np.zeros((height, width, 3))

for h in range(height):
    for w in range(width):
        input_neurons = pixels_array[h][w] / 255         

        for e in range(5):
            hidden_neurons_1[e] = np.dot(input_neurons, hidden_weights_1[e])

        for e in range(5):
            hidden_neurons_2[e] = sigmoid(np.dot(hidden_neurons_1, hidden_weights_2[e]))
        
        print(str(h) + " " + str(w))
        output = sigmoid( np.dot(hidden_neurons_2, hidden_weights_out) )
        output_array[h][w] = output * 255

image1.show()
image2.show()
im3 = Image.fromarray(np.int8(output_array), "RGB")
im3.show()