import numpy as np
from PIL import Image, ImageDraw
from matplotlib import cm 

image1 = Image.open("/home/gordeyb/input-test.jpg")
width = image1.size[0]
height = image1.size[1]

pixels_array = np.asarray(image1)

'''
for i in range(height):
    for j in range(width):
        output_array[i][j] = pixels_array[i][j]
        print(str(i) + " " + str(j))

print(output_array)
print(pixels_array)
'''

print(width)
print(height)

output_array = np.zeros((height, width, 3))

for h in range(height):
    for w in range(width):
        for r in range(3):
            output_array[h][w][r] = pixels_array[h][w][r]

print(output_array)

im = Image.fromarray(np.int8(output_array), "RGB")
im.show()
im2 = Image.fromarray(np.float_(pixels_array), "RGB")
im2.show()