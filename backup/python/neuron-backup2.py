import numpy as np
from PIL import Image, ImageDraw
from matplotlib import cm 

def sigmoid(x):
    return 1/(1 + np.exp(-x))

def activation(x):
    a = sigmoid(x)
    return a * (1 - a)

image1 = Image.open("/home/gordeyb/input-test.jpg")
width = image1.size[0]
height = image1.size[1]
pixels_array = np.asarray(image1)

image2 = Image.open("/home/gordeyb/out-test.jpg")
graypixels_array = np.asarray(image2)

#f = open("/home/gordeyb/w-data.txt", 'w')

input_neurons = np.zeros(3)
output_training = np.zeros(3)
output = np.zeros(3)
error = np.zeros(3)
hidden_neurons = np.zeros(10)
hidden_neurons_error = np.zeros(10)
hidden_weights_in = np.random.random((10, 3)) - 0.5
hidden_weights_out = np.random.random((10, 3)) - 0.5

for i in range(1):

    #print("Результат: " + str(output_training) + "\t" + str(output) + "\tОшибка:" + str(error))
    #print(str(i) + "\t" + str(output_pixel) + "\t" + str(output))
    
    for j in range(height):
        for k in range(width):
            # входной пиксель
            input_neurons = pixels_array[j][k] / 255

            # что мы хотим получить
            output_training = graypixels_array[j][k] / 255

            for e in range(10):
                hidden_neurons[e] = np.dot(input_neurons, hidden_weights_in[e])
            
            output = sigmoid( np.dot(hidden_neurons, hidden_weights_out) )
            # ошибка выходного нейрона
            error = output_training - output
            #print(error)
            print(str(i) + " " + str(j) + " " + str(k))
            #f.write(str(error) + "\n")

            for e in range(10):
                hidden_neurons_error[e] = np.dot(hidden_weights_out[e], error)
     
            for e in range(10):
                hidden_weights_in[e] = hidden_weights_in[e] + np.dot(hidden_neurons_error[e] * activation(hidden_neurons[e]) * 0.5, input_neurons)
            
            for e in range(10):    
                for b in range(3):            
                    hidden_weights_out[e][b] = hidden_weights_out[e][b] + error[b] * activation(output[b]) * hidden_neurons[e] * 0.5
'''
f.write(str(hidden_weights_in))
f.write("\n")
f.write(str(hidden_weights_out))

hidden_weights_in = np.array([[-3.07953929,  1.77251973, -7.61126544],
 [ 2.76726346, -2.94264016,  4.13458534],
 [ 8.5459728,  -1.98029862, 12.3498259 ],
 [-4.21911241,  2.98949169, -4.02437388],
 [-4.19377762,  1.69696042, -9.60528801],
 [-3.61106992,  2.87681226, -3.74466405],
 [ 8.13989519, -1.53363492, 12.81357102],
 [-1.23861762,  2.8921353, -4.56263653],
 [ 1.10479814, -1.24222971,  1.03133153],
 [ 2.37742317, -2.90486582,  3.68971168]])

hidden_weights_out = np.array([[-1.03943567, -1.07133669, -1.08289403],
 [-1.02228146, -1.01280165, -1.18227614],
 [-1.46040504, -1.5363351, -1.40407865],
 [ 1.24443471,  1.29713892,  1.4223808 ],
 [-1.22240746, -1.28163073, -1.30772839],
 [ 1.06869901,  1.09930541,  1.24113997],
 [-0.45603832, -0.43900018, -0.35724707],
 [ 0.86213074,  0.82666155,  1.00221238],
 [-0.20683627, -0.18884678, -0.26333465],
 [-0.84299948, -0.80489803, -0.98887037]])

'''
output_array = np.zeros((height, width, 3))

for h in range(height):
    for w in range(width):
        input_neurons = pixels_array[h][w]
        for e in range(10):
            hidden_neurons[e] = np.dot(input_neurons, hidden_weights_in[e]) / 255
        output_array[h][w] = sigmoid( np.dot(hidden_neurons, hidden_weights_out) ) * 255
        #print(str(i) + " " + str(j))

print(height)
print(width)

im = Image.fromarray(pixels_array, "RGB")
im.show()
im2 = Image.fromarray(graypixels_array, "RGB")
im2.show()
im3 = Image.fromarray(np.int8(output_array), "RGB")
im3.show()