import numpy as np
from PIL import Image, ImageDraw
from matplotlib import cm 

def sigmoid(x):
    return 1/(1 + np.exp(-x))

def activation(x):
    a = sigmoid(x)
    return a * (1 - a)

image1 = Image.open("/home/gordeyb/input-test.jpg")
width = image1.size[0]
height = image1.size[1]
pixels_array = np.asarray(image1)

image2 = Image.open("/home/gordeyb/output-test2.jpg")
graypixels_array = np.asarray(image2)

f = open("/home/gordeyb/w-data-backup.txt", 'w')

#np.random.seed(0)

input_neurons = np.zeros(3)
output_training = np.zeros(3)
output = np.zeros(3)
error = np.zeros(3)
hidden_neurons_1 = np.zeros(10)
hidden_neurons_2 = np.zeros(10)
hidden_neurons_error_1 = np.zeros(10)
hidden_neurons_error_2 = np.zeros(10)
hidden_weights_1 = np.random.random((10, 3)) - 0.5
hidden_weights_2 = np.random.random((10, 10)) - 0.5
hidden_weights_out = np.random.random((10, 3)) - 0.5

for i in range(1):    
    for j in range(height):
        print(str(i) + " " + str(j))
        for k in range(width):
            # входной пиксель
            input_neurons = pixels_array[j][k] / 255

            # что мы хотим получить
            output_training = graypixels_array[j][k] / 255

            hidden_neurons_1 = np.dot(hidden_weights_1, input_neurons)
            for e in range(10):
                hidden_neurons_2[e] = sigmoid(np.dot(hidden_weights_2[e], hidden_neurons_1))

            for e in range(10):
                for o in range(3):
                    output[o] += hidden_weights_out[e][o] * hidden_neurons_2[e]
            for e in range(3):
                output[e] = sigmoid(output[e])
            # ошибка выходного нейрона
            error = output_training - output
            hidden_neurons_error_2 = np.dot(hidden_weights_out, error)
            #print(str(j) + " " + str(i) + "\tРезультат: " + str(output_training) + "\t" + str(output) + "\tОшибка:" + str(error))
            print(hidden_weights_2)
            hidden_neurons_error_1 = np.dot(hidden_weights_2, hidden_neurons_error_2)

            #for e in range(10):
            #    hidden_weights_1[e] = hidden_weights_1[e] + np.dot(hidden_neurons_error_1[e] * activation(hidden_neurons_1[e]) * 0.5, input_neurons)
            
            for e in range(10):
                for b in range(3):
                    hidden_weights_1[e][b] = hidden_weights_1[e][b] + hidden_neurons_error_1[e] * activation(hidden_neurons_1[e]) * 0.5 * input_neurons[b]

            for e in range(10):
                for b in range(10):
                    hidden_weights_2[e][b] = hidden_weights_2[e][b] + hidden_neurons_error_2[e] * activation(hidden_neurons_2[e]) * 0.5 * hidden_neurons_1[b]
            
            #for e in range(10):
            #    hidden_weights_2[e] = hidden_weights_2[e] + np.dot(hidden_neurons_error_2[e] * activation(hidden_neurons_2[e]) * 0.5, hidden_neurons_1)

            for e in range(10):    
                for b in range(3):           
                    hidden_weights_out[e][b] = hidden_weights_out[e][b] + error[b] * activation(output[b]) * hidden_neurons_2[e] * 0.5

f.write(str(hidden_weights_1))
f.write("\n")
f.write(str(hidden_weights_2))
f.write("\n")
f.write(str(hidden_weights_out))
'''
hidden_weights_1 = np.array([[-316.95466362,-362.78218556,-355.8547188],
[-898.23895132,-224.60325049,-681.38657472],
[-202.81873098,-293.71029691,-343.89365425],
[49.99815415,114.41496304,156.30002335],
[734.36399955,903.74102366,1363.64430809],
[-331.42295213,-309.78903823,-339.76190919],
[-27.92006889,1059.68554561,782.38809971],
[-802.88431657,-588.79330269,-429.44274085],
[-413.95457726,-353.22290789,-362.95372654],
[425.80951787,379.24144243,468.54524301]])

hidden_weights_2 = np.array([[615436.84501816,311134.27851818,170934.18678794,-206844.06713187
,-1431831.73117642,333495.94293896,-844059.37576709,771953.24011558
,846223.14113271,-705987.30669341]
,[394783.31516368,200104.02032476,110333.74508868,-132261.4302701
,-916924.00092394,214427.49541952,-540271.48976396,496654.9483653
,542327.33913613,-452093.05641459]
,[395589.98621225,199908.29873545,110385.02911358,-132575.22886411
,-918401.96478489,214888.82400084,-541129.47625414,497310.45500409
,543599.92295636,-452884.70331548]
,[31092.2862674,53009.78765436,25413.60178988,-8516.63825963
,-86633.39490252,28746.53094835,-52382.18121183,56532.58835603
,33004.72176557,-37473.63473881]
,[654278.44955503,329404.83546478,182569.24245599,-218989.3119503
,-1516522.16397899,355751.3190796,-893396.67573034,822537.72349911
,899041.89530542,-747970.29068796]
,[373514.7456368,188263.30415962,102767.14163791,-126129.62285574
,-871459.21548387,201598.45846754,-514065.78322454,466210.38020643
,514116.14421137,-429579.65145023]
,[462548.90131378,233084.20444363,128861.22546423,-155019.61355508
,-1073306.45099519,251239.47119389,-632400.52981805,580992.76492755
,635695.49070393,-529303.9939951]
,[434170.36072647,217740.09763569,118165.7849408,-147308.13392847
,-1015479.15504564,233370.34612505,-599457.91911519,538943.2523354
,598333.69909738,-500499.5696402]
,[563449.09280195,283267.09203042,156001.62764444,-189388.32662593
,-1309596.80870174,305276.02716599,-771997.87250889,705544.90658106
,774955.97870323,-645781.73455684]
,[236980.75392946,122304.74918477,67006.13835376,-79281.15296885
,-551871.54812346,128867.14248797,-325138.42548915,299959.16495984
,325176.73824698,-272001.7907883]])

hidden_weights_out = np.array([[-1.00363443,-0.25945493,-0.60911564],
[-0.65849554,-0.25540001,-0.38566102],
[-0.65290893,-0.18163158,-0.29080436],
[-1.00492704,-1.32611538,-1.51435426],
[-1.07201689,-0.1653301,-0.21576701],
[-0.58898084,-0.04767616,-0.41143691],
[-0.75566604,-0.12942117,-0.23201989],
[-0.65590478,0.12785344,-0.45656145],
[-0.90008685,-0.03811538,-0.30024549],
[-0.42257333,-0.43186787,-0.57077947]])
'''
output_array = np.zeros((height, width, 3))

for h in range(height):
    for w in range(width):
        newOutput = np.zeros(3)
        inputPixel = pixels_array[h][w] / 255
        hidden_neurons_1 = np.dot(hidden_weights_1, inputPixel)
        for e in range(10):
            hidden_neurons_2[e] = sigmoid(np.dot(hidden_weights_2[e], hidden_neurons_1))

        for e in range(10):
            for o in range(3):
                newOutput[o] += hidden_weights_out[e][o] * hidden_neurons_2[e]
        for e in range(3):
            newOutput[e] = sigmoid(newOutput[e])

        output_array[h][w] = newOutput * 255
        #print(str(h) + " " + str(w) + " " + str(output_array[h][w]))

print(height)
print(width)

im = Image.fromarray(pixels_array, "RGB")
im.show()
im2 = Image.fromarray(graypixels_array, "RGB")
im2.show()
im3 = Image.fromarray(np.int8(output_array), "RGB")
im3.show()