#include <iostream>
#include <cmath>
#include <ctime>
#include <cstring>
#include <random>

std::random_device rd;

double rand_d(double min, double max){
    std::uniform_real_distribution<double> uid(min, max);
    return uid(rd);
}

double sigmoid(double x){
    return 1 / (1 + exp(-x));
}

int main(){
    double training_inputs[5][3] = {
        {0, 0, 1},
        {1, 1, 1},
        {1, 0, 1},
        {0, 1, 1},
        {0, 0, 0},
    };
  
    double training_outputs[5] = {0.0,1.0,1.0,0.0,0.0};

    srand(std::time(NULL));
    double synaptic_weights[3];
    for(int i = 0; i < 3; i++)
        synaptic_weights[i] = rand_d(-1.0, 1.0);

    double input_layer[5][3];
    memcpy(input_layer, training_inputs, 3*4*sizeof(double));
    double outputs[5];

    for(int i = 0; i < 40000; i++){
        for(int k = 0; k < 5; k++){
            outputs[k] = sigmoid(training_inputs[k][0] * synaptic_weights[0] + 
                                training_inputs[k][1] * synaptic_weights[1] +
                                training_inputs[k][2] * synaptic_weights[2]);

        }

        double err[5];
        for(int k = 0; k < 5; k++){
            err[k] = training_outputs[k] - outputs[k];
        }

        double adjustments[3] = {0.0, 0.0, 0.0};
        double a[5] = {
            outputs[0] * (1.0 - outputs[0]) * err[0],
            outputs[1] * (1.0 - outputs[1]) * err[1],
            outputs[2] * (1.0 - outputs[2]) * err[2],
            outputs[3] * (1.0 - outputs[3]) * err[3],
            outputs[4] * (1.0 - outputs[4]) * err[4],
        };
        for(int k = 0; k < 3; k++){
            for(int j = 0; j < 5; j++){
                adjustments[k] += training_inputs[j][k] * a[j];
            }
            synaptic_weights[k] += adjustments[k];
        }
    }

    std::cout << "Веса после обучения" << std::endl;
    for(int i = 0; i < 3; i++)
        std::cout << synaptic_weights[i] << std::endl;
   
    std::cout << "Результат после обучения" << std::endl;
    for(int i = 0; i < 5; i++){
        std::cout << outputs[i] << std::endl;
    }

    std::cout << "Новая ситуация" << std::endl;
    double inputs[3] = {0,0,0};
    double new_output;
    new_output = sigmoid(inputs[0] * synaptic_weights[0] + inputs[1] * synaptic_weights[1] + inputs[2] * synaptic_weights[2]);
    std::cout << new_output << std::endl;
    return 0;
}
