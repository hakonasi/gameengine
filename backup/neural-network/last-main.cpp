#include <iostream>
#include <cmath>
#include <ctime>
#include <cstring>
#include <random>

std::random_device rd;

double rand_d(double min, double max){
    std::uniform_real_distribution<double> uid(min, max);
    return uid(rd);
}

double sigmoid(double x){
    return 1 / (1 + exp(-x));
}

int main(){
    double training_inputs[4][3] = {
        {0, 0, 1},
        {1, 1, 1},
        {1, 0, 1},
        {0, 1, 1},
    };
  
    double training_outputs[4] = {0,1,1,0};

    srand(std::time(NULL));
    double synaptic_weights[3];
    for(int i = 0; i < 3; i++)
        synaptic_weights[i] = rand_d(-1.0, 1.0);

    std::cout << "Синапсы" << std::endl;
    std::cout   << synaptic_weights[0] << std::endl
                << synaptic_weights[1] << std::endl
                << synaptic_weights[2] << std::endl;

    double input_layer[4][3];
    memcpy(input_layer, training_inputs, 3*4*sizeof(double));
    double outputs[4];

    for(int i = 0; i < 5000; i++){
        for(int k = 0; k < 4; k++){
            outputs[k] = sigmoid(training_inputs[k][0] * synaptic_weights[0] + 
                                training_inputs[k][1] * synaptic_weights[1] +
                                training_inputs[k][2] * synaptic_weights[2]);
        }

        double err[4];
        for(int k = 0; k < 4; k++){
            err[k] = training_outputs[k] - outputs[k];
        }

        double adjustments[3];
        double a[4] = {
            outputs[0] * (1 - outputs[0]) * err[0],
            outputs[1] * (1 - outputs[1]) * err[1],
            outputs[2] * (1 - outputs[2]) * err[2],
            outputs[3] * (1 - outputs[3]) * err[3],
        };
        for(int k = 0; k < 3; k++){
            for(int j = 0; j < 4; j++){
                adjustments[k] += training_inputs[j][k] * a[j];
            }
            synaptic_weights[k] += adjustments[k];
        }
    }

    std::cout << "Веса после обучения" << std::endl;
    for(int i = 0; i < 3; i++)
        std::cout << synaptic_weights[i] << std::endl;

    std::cout << "Результат" << std::endl;
    for(int i = 0; i < 4; i++){
        std::cout << outputs[i] << std::endl;
    }
    return 0;
}
