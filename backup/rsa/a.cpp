#include <iostream>
#include <boost/multiprecision/cpp_int.hpp>

namespace mp = boost::multiprecision;

int main(){
    mp::cpp_int a = 2;
    std::cout << mp::pow(a, 2048) << std::endl;
    return 0;
}
