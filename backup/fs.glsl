#version 330 core

in vec2 uv;
uniform sampler2D diffuse;

out vec3 color;

struct Material{
    vec3 color;
};

uniform Material material;

void main(){
//    color = texture(diffuse, uv).rgb;
    color = material.color;
}
