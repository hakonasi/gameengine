lua c++ api. функция вывода ключей и значений таблицы.
 
int pri(lua_State *L, const char t[] ) {// передаем указатель на состояние.
    lua_getglobal(L, t);// получить значение глобальной переменной, таблица.
    lua_assert(lua_istable(L, -1));// проверить является 1 элемент стека таблицей.
    if (LUA_TTABLE == lua_type(L, -1)) {// это таблица.
    lua_pushnil(L);//кладем на вершину стека NULL
  while (lua_next(L, 1) != 0) {/* получает ключ из стека и отправляет пару ключ - значение из таблицы.
      Ключ имеет - 2, а значение - 1.*/
        if (LUA_TSTRING == lua_type(L, -2)) {// если ключ строка.
        if (LUA_TNUMBER == lua_type(L, -1)) {// значение число.
        cout << "key " << lua_tostring(L, -2) << " value " << lua_tonumber(L, -1) << endl;}
        lua_pop(L, 1);// удалить n элементы из стека.
        }
    }
}
return 1;// вернуть 1.
};
int main() {
    lua_State *L = luaL_newstate();
    luaL_openlibs(L);
    checklua(L, "main.lua");// Загружает и запускает заданный файл. файл в которым все происходит.
    pri(L,"t");// вывести ключи и значения таблицы.
    lua_pcall(L, 0, 0, 0);// вызов функции в lua файле. 1 параметр кол-во передаваемых, кол-во возвращаемых, обработчик ошибок.
    lua_close(L);// закрыть состояние
    cin.get();//ожидает ввода символа программа завершается.
    return 0;}
 
lua
 
t= {x=10, y=20, z=30}
t1= {x=1, y=2, z=3}
 
выводится.
key y value 20
key x value 10
key z value 30
 
 
 
int pri(lua_State *L, const char t[]) {// передаем указатель на состояние.
    lua_getglobal(L, t);// получить значение глобальной переменной, таблица.
    lua_assert(lua_istable(L, -1));// проверить является 1 элемент стека таблицей.
    if (LUA_TTABLE == lua_type(L, -1)) {// это таблица.
        lua_pushnil(L);//кладем на вершину стека NULL
        while (lua_next(L, 1) != 0) {/* получает ключ из стека и отправляет пару ключ - значение из таблицы.
            Ключ имеет - 2, а значение - 1.*/
            if (LUA_TSTRING == lua_type(L, -2)) {// если ключ строка.
                if (LUA_TNUMBER == lua_type(L, -1)) {// значение число.
                    cout << "key " << lua_tostring(L, -2) << " value " << lua_tonumber(L, -1) << endl;
                }
                lua_pop(L, 1);// удалить n элементы из стека.
            }
        }
    }cout << "\n";
    return 1;// вернуть 1.
}
 
int addtab(lua_State *L, const char t[], const char key[], int value) {// передаем указатель на состояние.
    lua_getglobal(L, t);// получить значение глобальной переменной, таблицей.
    lua_pushnumber(L, value);// отправить значение в таблицу..
    lua_setfield(L, -2, key);// уст ключ для него.
    lua_getglobal(L, t);// получить значение глобальной переменной, таблицей.
    lua_getfield(L, -1, key);// отправить ключ в таблицу.
    return 0;// вернуть 1.
}
int main() {
    lua_State *L = luaL_newstate();
    //checklua(L, "main.lua");// Загружает и запускает заданный файл. файл в которым все происходит.
 
    luaL_dofile(L, "main.lua");// запуск файла, в которым все происходит.   
    pri(L, "t");// вывести ключи и значения таблицы.
    addtab(L, "t", "a", 1000);// уст ключ и значение для таблицы 
    pri(L, "t");// вывести ключи и значения таблицы.
    addtab(L, "t", "b", 99);// уст ключ и значение для таблицы 
    pri(L, "t");// вывести ключи и значения таблицы.
    lua_pcall(L, 0, 0, 0);// вызов функции в lua файле. 1 параметр кол-во передаваемых, кол-во возвращаемых, обработчик ошибок.
    lua_close(L);// закрыть состояние
    cin.get();//ожидает ввода символа программа завершается.
    return 0;
}
 
 
lua
 
t= {x=10, y=20, z=30}
t1= {x=1, y=2, z=3}
 
выводится.
 
key z value 30
key y value 20
key x value 10
 
key z value 30
key y value 20
key x value 10
key a value 1000
 
key x value 10
key z value 30
key a value 1000
key b value 99
key y value 20
 
вариант 2.
 
 
void getstack(lua_State* L, int pos) {
    if (LUA_TSTRING == lua_type(L, pos)) {
        cout << lua_tostring(L, pos);
    }
    if (LUA_TNUMBER == lua_type(L, pos)) {
        double x = lua_tonumber(L, pos);
        int x2 = (int)x;
        if (x == x2) { cout << x2; }
        else { cout << x; }
    }
    if (LUA_TBOOLEAN == lua_type(L, pos)) {
        cout << (lua_toboolean(L, pos) ? "true " : "false ");
    }
    if (LUA_TNIL == lua_type(L, pos)) {
        cout << "not /n";
    }
    if (LUA_TTABLE == lua_type(L, pos)) {
        cout << "LUA_TTABLE " << endl;
    }
    if (LUA_TFUNCTION == lua_type(L, pos)) {
        cout << "LUA_TFUNCTION " << endl;
    }
    if (LUA_TLIGHTUSERDATA == lua_type(L, pos)) {
        cout << "LIGHTUSERDATA " << endl;
    }
    if (LUA_TTABLE == lua_type(L, pos)) {
        cout << "LUA_TTABLE " << endl;
    }
    if (LUA_TFUNCTION == lua_type(L, pos)) {
        cout << "LUA_TFUNCTION " << endl;
    }
    if (LUA_TUSERDATA == lua_type(L, pos)) {
        cout << "LUA_TUSERDATA " << endl;
    }
    if (LUA_TNIL == lua_type(L, pos)) { // если x нет.
        cout << "nill " << endl;
    }
};
int pri(lua_State* L, const char t[]) {// передаем указатель на состояние.
    lua_getglobal(L, t);// получить значение глобальной переменной, таблица.
    lua_assert(lua_istable(L, -1));// проверить является 1 элемент стека таблицей.
    if (LUA_TTABLE == lua_type(L, -1)) {// это таблица.
        lua_pushnil(L);//кладем на вершину стека NULL
        while (lua_next(L, 1) != 0) {/* получает ключ из стека и отправляет пару ключ - значение из таблицы.
            Ключ имеет - 2, а значение - 1.*/
            cout << "key "; getstack(L, -2); cout << " value "; getstack(L, -1); cout << endl;
            lua_pop(L, 1);// удалить n элементы из стека.
        }
        
    }
return 0;// вернуть 1.
};
int main() {
    lua_State* L = luaL_newstate();
    luaL_openlibs(L);
    luaL_dofile(L, "main.lua");// Загружает и запускает заданный файл. файл в которым все происходит.
    pri(L, "t");// вывести ключи и значения таблицы.
    lua_close(L);// закрыть состояние
    return 0;
};
 
 
 
 
 
lua c++ api. Работа с таблицей.
 
int main() {
    lua_State *L = luaL_newstate();
    luaL_openlibs(L);//открыть все стандартные библиотеки lua.
    checklua(L, "main.lua");// Загружает и запускает заданный файл. файл в которым все происходит.
    lua_getglobal(L, "t");// получить значение глобальной переменной, таблица.
    lua_assert(lua_istable(L, 1));// проверить является 1 элемент стека таблицей.
    lua_pushstring(L, "x");// отправить ключ x в стек.
    lua_gettable(L, 1);// получить значение по ключу из стека.
    lua_assert(lua_isnumber(L, 2));// проверить является значение 2 элемента стека число.  
    int x = lua_tonumber(L, 2);
    lua_getglobal(L, "t");// получить значение глобальной переменной таблица.
    lua_pushstring(L, "y");
    lua_gettable(L, 1);
    lua_assert(lua_isstring(L, 4));
    string y = lua_tostring(L, 4);
    lua_getglobal(L, "t");// получить значение глобальной переменной, таблицей.
    lua_getfield(L, 5, "z");// отправить ключ в таблицу.
    lua_assert(lua_isboolean(L, 5));
    bool z = lua_toboolean(L, 5);// получить булевое значение по ключу.
    lua_getglobal(L, "t");// получить значение глобальной переменной, таблицей.
    lua_pushstring(L, "dog");// отправить значение в таблицу.
    lua_setfield(L, 5, "a");// уст ключ для него.
    lua_getglobal(L, "t");// получить значение глобальной переменной, таблицей.
    lua_getfield(L, 5, "a");// отправить ключ в таблицу.
    lua_assert(lua_isstring(L, 5));
    string a = lua_tostring(L, -1);
    cout << "x = " << x << "\ny = " << y << "\nz = " << z << "\na = " << a << endl;
    lua_close(L);// закрыть состояние
 
    cin.get();//ожидает ввода символа программа завершается.
 
    return 0;
}
 
Lua
 
t= {x=300,y="ok you y",z=true}
 
Сохранение всех ключей таблицы в реестре.
 
int pritab(lua_State* L, const char t[]) {
    int x = 0;
    lua_getglobal(L, t);// получить значение глобальной переменной, таблица.
    lua_pushvalue(L, -1);
    lua_pushnil(L);
    while (lua_next(L, -2)) {
    //. cout << " key ";
        lua_pushvalue(L, -2); 
        lua_rawseti(L, LUA_REGISTRYINDEX, x);
        //getstack(L, -2);
        lua_pushvalue(L, -2);
        //cout << "key "; getstack(L, -1); cout << " value "; getstack(L, -2); cout << endl;
        lua_pop(L, 2);
        x++;
    }
    lua_pop(L, 1);
    pushlua(L, x);
    return 1;
};
int main() {
    const char* LUA = R"(
t= {x=10, y=20, z=30}
)";
 
    lua_State* L = luaL_newstate();
    luaL_openlibs(L);
 
    checkerror(L, LUA);//Функция проверка на ошибки.
    pritab(L, "t");// вывести ключи и значения таблицы.
    int len = lua_tointeger(L,-1);
    for (int i = 0; i < len; i++){
    lua_rawgeti(L, LUA_REGISTRYINDEX, i);/* вернуть значение для ключа в реестр. */
    cout << "key "; getstack(L, -1);  cout << endl;}
    lua_close(L);// закрыть состояние
    return 0;
};
 
Вывод всех значений ключей из реестра.
 
int pritab(lua_State* L, const char t[]) {
    int x = 0;
    lua_getglobal(L, t);// получить значение глобальной переменной, таблица.
    lua_pushvalue(L, -1);
    lua_pushnil(L);
    while (lua_next(L, -2)) {
        //. cout << " key ";
        lua_pushvalue(L, -2);
        lua_rawseti(L, LUA_REGISTRYINDEX, x);
        //getstack(L, -2);
        lua_pushvalue(L, -2);
        //cout << "key "; getstack(L, -1); cout << " value "; getstack(L, -2); cout << endl;
        lua_pop(L, 2);
        x++;
    }
    lua_pop(L, 1);
    pushlua(L, x);
    return 1;
};
int main() {
    const char* LUA = R"(
t= {x=10, y=20, z=30}
)";
 
    lua_State* L = luaL_newstate();
    luaL_openlibs(L);
 
    checkerror(L, LUA);//Функция проверка на ошибки.
    pritab(L, "t");// вывести ключи и значения таблицы.
    int len = lua_tointeger(L, -1);
    lua_pop(L, 1);
    for (int i = 0; i < len; i++) {
        lua_rawgeti(L, LUA_REGISTRYINDEX, i);/* вернуть значение для ключа в реестр. */
                
        cout << "key "; getstack(L, -1);
 
        lua_getglobal(L, "t"); 
        lua_pushvalue(L, -2);
        lua_gettable(L, -4);
        cout << " value "; getstack(L, -1);  cout << endl;
        lua_pop(L, 3);
 
    }
    lua_close(L);// закрыть состояние
    return 0;
};
