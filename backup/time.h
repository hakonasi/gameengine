#ifndef TIME_H
#define TIME_H

#include "window.h"

class Time{
public:
    Time();
    ~Time();

    float GetDeltaTime();
    void Update();
private:
    float deltaTime;
    float lastTime;
};

#endif
