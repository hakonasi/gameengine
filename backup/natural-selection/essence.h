#ifndef ESSENCE_H
#define ESSENCE_H

#include <iostream>
#include <vector>
#include <chrono>
#include <cmath>

#include "vector.h"

using namespace std;
using namespace chrono;

class Essence{
public:
    Essence(unsigned int map_size);
    ~Essence();

    unsigned int GetSpeed();
    unsigned int GetSensitivity();
    nature::vector GetPosition();

    void SetMap(std::vector<std::vector<bool>>& world);
    void SetSpeed(unsigned int speed);
    void SetSensitivity(unsigned int sensitivity);
    void SetPosition(nature::vector position);
    bool IsFoodFounded(); 
    void Movement();
    void RandMovement();
private:
    unsigned int speed; // количество шагов за секунду
    unsigned int sensitivity;
    unsigned int map_size;
    
    bool isFoodFounded = false;
     
    nature::vector position;
    nature::vector foodCoord;

    std::vector<std::vector<bool>> world;
};

#endif
