#include "essence.h"

Essence::Essence(unsigned int map_size){
    this->map_size = map_size;

    srand(std::time(0));

    position.x = (rand() % (map_size-1));
    position.y = (rand() % (map_size-1));
    speed = (rand() % (map_size-1)/2) + 1;
    sensitivity = (rand() % (map_size-1)/2) + 1;
    std::cout << "speed is " << speed << std::endl;
    std::cout << "sensitivity is " << sensitivity << std::endl;
    std::cout << "position is " << position.x << ";" << position.y << std::endl;
}

Essence::~Essence(){

}

unsigned int Essence::GetSpeed(){
    return speed;
}

unsigned int Essence::GetSensitivity(){
    return sensitivity;
}

nature::vector Essence::GetPosition(){
    return position;
}

void Essence::SetSpeed(unsigned int speed){
    this->speed = speed;
}

void Essence::SetSensitivity(unsigned int sensitivity){
    this->sensitivity = sensitivity;
}

void Essence::SetPosition(nature::vector position){
    this->position = position;
}

void Essence::SetMap(std::vector<std::vector<bool>>& world){
    this->world = world;
}

void Essence::RandMovement(){
    srand(std::time(0));
    int dir = rand() % 7;
    // 0: x++
    // 1: x--
    // 2: y--
    // 3: y++
    switch(dir)
    {
        case 0:
            if(position.x < map_size-2){
                position.x++;
            }
            else{
                dir = 1;
            }
            break;
        case 1:
            if(position.x > 0){
                position.x--;
            }
            else{
                dir = 2;
            }
            break;
        case 2:
            if(position.y > 0){
                position.y--;
            }
            else{
                dir = 3;
            }
            break;
        case 3:
            if(position.y < map_size-2){
                position.y++;
            }
            break;
        case 4:
            if(position.x > 0 && position.y > 0){
                position.x--;
                position.y--;
            }
            else{
                dir = 7;
            }
            break;
        case 5:
            if(position.x < map_size-2 && position.y > 0){
                position.x++;
                position.y--;
            }
            else{
                dir = 7;
            }
            break;
        case 6:
            if(position.x < map_size-2 && position.y < map_size-2){
                position.x++;
                position.y++;
            }
            else{
                dir = 1;
            }
            break;
        case 7:
            if(position.x > 0 && position.y < map_size-2){
                position.x--;
                position.y++;
            }
            break;
    }
}

void Essence::Movement(){
    auto start = high_resolution_clock::now();
    
    int squareSize = 3;
        while(!isFoodFounded && squareSize <= sensitivity){
            for(int x = -1; x < sensitivity-1; x++){
                int y = -1;
                if(x+position.x < 0 || x+position.x >= map_size || y+position.y < 0 || y+position.y >=map_size){
                    continue;
                }
                else{
                    if(world[x+position.x][y+position.y] == true){
                        foodCoord.x = x+position.x;
                        foodCoord.y = y+position.y;
                        isFoodFounded = true;
                        break;
                    }
                }
            }
            for(int y = -1; y < sensitivity-1; y++){
                int x = 1;
                if(x+position.x < 0 || x+position.x >= map_size || y+position.y < 0 || y+position.y >=map_size){
                    continue;
                }   
                else{
                    if(world[x+position.x][y+position.y] == true){
                        foodCoord.x = x+position.x;
                        foodCoord.y = y+position.y;
                        isFoodFounded = true;
                        break;
                    }
                }
            }
            for(int x = -1; x < sensitivity-1; x++){
                int y = 1;
                if(x+position.x < 0 || x+position.x >= map_size || y+position.y < 0 || y+position.y >=map_size){
                    continue;
                }
                else{
                    if(world[x+position.x][y+position.y] == true){
                        foodCoord.x = x+position.x;
                        foodCoord.y = y+position.y;
                        isFoodFounded = true;
                        break;
                    }
                }
            }
            for(int y = -1; y < sensitivity-1; y++){
                int x = -1;
                if(x+position.x < 0 || x+position.x >= map_size || y+position.y < 0 || y+position.y >=map_size){
                    continue;
                }
                else{
                    if(world[x+position.x][y+position.y] == true){
                        foodCoord.x = x+position.x;
                        foodCoord.y = y+position.y;
                        isFoodFounded = true;
                        break;
                    }
                }
            }
            if(!isFoodFounded){
                squareSize++;
            }       
        }
    if(!isFoodFounded){
        std::cout << "preX: " << position.x << " preY: " << position.y << std::endl;
        RandMovement();
        return;
        std::cout << "X: " << position.x << " Y: " << position.y << std::endl;
    }

    while(isFoodFounded){
        if(duration_cast<milliseconds>(high_resolution_clock::now()-start).count() >= 1000/speed)
        {
            // перемещение.
            // Если в радиусе чуствительности сущность "нашла" еду, тогда она (сущность) идёт к ней.
            // Если же сущность не нашла еду, тогда она движется в случайных направлениях, 
            // пока еда не попадёт в радиус чуствительности.
            if(isFoodFounded){
                int x;
                if(position.x > foodCoord.x)
                    x = position.x-1;
                else
                    x = position.x+1;
                int y = (((x-position.x)*(foodCoord.y-position.y))/(foodCoord.x-position.x)) + position.y;
                position.x = x;
                position.y = y;
                std::cout << position.x << ";" << position.y << std::endl;
    
                if(position.x == foodCoord.x && position.y == foodCoord.y){
                    isFoodFounded = false;
                }
            }
            start = high_resolution_clock::now();
        }
    }
}

bool Essence::IsFoodFounded(){
    return isFoodFounded;
}
