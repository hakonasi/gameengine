// This program was writed by Gordey Balaban (balgor36)
// 28.12.2019

#include "essence.h"

#define MAP_SIZE 10
#define FOOD_SPAWN 30 // какое процентное количество карты будет заполнено едой

std::vector<std::vector<bool>> world;
float food_count = 0.0f; // количество заспауненой еды

Essence nature_a(MAP_SIZE); // :))

void CoutWorld(){
    for(unsigned int j = 0; j < MAP_SIZE; j++){
        for(unsigned int i = 0; i < MAP_SIZE; i++){
            if(nature_a.GetPosition().x == i && nature_a.GetPosition().y == j){
                std::cout << "[X]";
            }
            else
                std::cout << "[" << world[i][j] << "]";
            if(i == 7 && j == 2){
                std::cout << "[;]";
            }
        }
        std::cout << std::endl;
    }
}

int main(){
    food_count = ((float)MAP_SIZE*MAP_SIZE)*((float)FOOD_SPAWN/100.0f);

    std::cout << "map size is " << MAP_SIZE << std::endl;
    std::cout << "food count is " << food_count << std::endl; 

    world.resize(MAP_SIZE);
    for(unsigned int i = 0; i < MAP_SIZE; i++){
        world[i].resize(MAP_SIZE);
    }

    unsigned int x = 0; 
    // заполняем карту едой
    for(unsigned int j = 0; j < MAP_SIZE; j++){
        for(unsigned int i = 0; i < MAP_SIZE; i++){
            // вычисляем, через какой промежуток нужно ставить еду
            if(x+1 < 100.0f/FOOD_SPAWN){
                world[i][j] = false; // здесь еды нет
                x++;
            }
            else{
                world[i][j] = true; // добавляем еду
                x = 0;
            }
        }
    }
    nature_a.SetMap(world);
    CoutWorld();
    std::cout << std::endl;
    nature_a.Movement();
    CoutWorld();

    return 0;
}
