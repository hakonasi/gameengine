#include "essence.h"

Essence::Essence(unsigned int map_size){
    this->map_size = map_size;

    srand(std::time(0));

    position.x = (rand() % (map_size-1));
    position.y = (rand() % (map_size-1));
    speed = (rand() % (map_size-1)/2) + 1;
    sensitivity = (rand() % (map_size-1)/2) + 1;
    std::cout << "speed is " << speed << std::endl;
    std::cout << "sensitivity is " << sensitivity << std::endl;
    std::cout << "position is " << position.x << ";" << position.y << std::endl;
}

Essence::~Essence(){

}

unsigned int Essence::GetSpeed(){
    return speed;
}

unsigned int Essence::GetSensitivity(){
    return sensitivity;
}

nature::vector Essence::GetPosition(){
    return position;
}

void Essence::SetSpeed(unsigned int speed){
    this->speed = speed;
}

void Essence::SetSensitivity(unsigned int sensitivity){
    this->sensitivity = sensitivity;
}

void Essence::SetPosition(nature::vector position){
    this->position = position;
}

void Essence::SetMap(std::vector<std::vector<bool>>& world){
    this->world = world;
}

void Essence::RandMovement(){
    srand(std::time(0));
    int dir = rand() % 3;
    // 0: x++
    // 1: x--
    // 2: y--
    // 3: y++
    std::cout << "dir movement: " << dir << std::endl;
    switch(dir)
    {
        case 0:
            if(position.x < map_size-1){
                position.x++;
            }
            break;
        case 1:
            if(position.x > 0){
                position.x--;
            }
            break;
        case 2:
            if(position.y < map_size-1){
                position.y--;
            }
            break;
        case 3:
            if(position.y > 0){
                position.y++;
            }
            break;
    }
}

void Essence::Movement(){
    auto start = high_resolution_clock::now();
    
    int foodX = position.x;
    float foodY = 0;
    int q = 1;

    if(position.x > foodX){
        q = -1;
    }
    std::cout << "q is " << q << std::endl;

    while(!isFoodFounded){
        for(foodX = position.x; foodX < foodX + sensitivity*q; foodX+=q){
            foodY = sqrt(position.y^2 - foodX^2 + 2*position.x*foodX + position.x^2 + position.y^2 + sensitivity^2);

            std::cout << "foodX: " << foodX << " foodY: " << foodY << std::endl;
            std::cout << "round(foodY): " << round(foodY) << std::endl;
    
            if(foodX < map_size){
                if(position.y + round(foodY) < map_size){
                    if(world[foodX][position.y + round(foodY)] == true){
                        foodY = position.y + round(foodY);
                        isFoodFounded = true;
                        break;
                    }
                }
                if(position.y - round(foodY) < map_size){
                    if(world[foodX][position.y - round(foodY)] == true){
                        foodY = position.y - round(foodY);
                        isFoodFounded = true;
                        break;
                    }        
                }    
            }
            else{
                break;
            }
        }
        std::cout << "good" << std::endl;
    }
    if(!isFoodFounded){
        RandMovement();
    }

    while(isFoodFounded){
        if(duration_cast<milliseconds>(high_resolution_clock::now()-start).count() >= 1000/speed)
        {
            // перемещение.
            // Если в радиусе чуствительности сущность "нашла" еду, тогда она (сущность) идёт к ней.
            // Если же сущность не нашла еду, тогда она движется в случайных направлениях, 
            // пока еда не попадёт в радиус чуствительности.
            if(isFoodFounded){
                int x;
                if(position.x > foodX)
                    x = position.x-1;
                else
                    x = position.x+1;
                int y = (((x-position.x)*(foodY-position.y))/(foodX-position.x)) + position.y;
                position.x = x;
                position.y = y;
                std::cout << position.x << ";" << position.y << std::endl;
    
                if(position.x == foodX && position.y == foodY){
                    isFoodFounded = false;
                }
            }
            start = high_resolution_clock::now();
        }
    }
}

bool Essence::IsFoodFounded(){
    return isFoodFounded;
}
