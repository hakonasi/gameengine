#ifndef IMAGE_H
#define IMAGE_H

#include "window.h"
#include <SOIL.h>

class Image{
public:
    Image();
    Image(const char* filename);

    void LoadImage(const char* filename);
    GLuint GetTexture();
private:
    GLuint CreateTexture(int width, int height, unsigned char* data);

    GLuint m_texture;
};

#endif
