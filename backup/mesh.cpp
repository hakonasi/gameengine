#include "mesh.h"

Mesh::Mesh(){

}

Mesh::Mesh(std::vector<glm::vec3>& vertices, std::vector<uint>& indices, std::vector<glm::vec2>& uvs){
    indices_size = indices.size();
    Init(vertices, indices, uvs);
}

Mesh::~Mesh(){

}

bool Mesh::ImportMesh(const char* filename){
    bool first = true;
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(filename, aiProcess_GenSmoothNormals | aiProcess_Triangulate | aiProcess_CalcTangentSpace | aiProcess_FlipUVs);

    if(scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode || !scene){
        printf("%s\n", importer.GetErrorString());
        return false;
    }
    RecursiveProcess(scene->mRootNode, scene, first);
    return true;
}

void Mesh::RecursiveProcess(aiNode* node, const aiScene* scene, bool& first){
    first = false;
    for(uint i = 0; i < node->mNumMeshes; i++){
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        numChilds = node->mNumChildren;
        ProcessMesh(mesh, scene, first);
        first = false;
    }

    for(uint i = 0; i < node->mNumChildren; i++){
        RecursiveProcess(node->mChildren[i], scene, first);
    }
}

void Mesh::ProcessMesh(aiMesh* mesh, const aiScene* scene, bool& first){
    std::vector<glm::vec3> vertices;
    std::vector<uint> indices;
    std::vector<glm::vec2> uvs;

    for(uint i = 0; i < mesh->mNumVertices; i++){
        glm::vec3 tempVec;
        tempVec.x = mesh->mVertices[i].x;
        tempVec.y = mesh->mVertices[i].y;
        tempVec.z = mesh->mVertices[i].z;
        vertices.push_back(tempVec);
    
        glm::vec2 tempUV;
        if(mesh->mTextureCoords[0]){
            tempUV.x = mesh->mTextureCoords[0][i].x;
            tempUV.y = mesh->mTextureCoords[0][i].y;
        }
        else{
            tempUV.x = 0;
            tempUV.y = 0;
        }
        uvs.push_back(tempUV);
    }
    for(uint i = 0; i < mesh->mNumFaces; i++){
        aiFace face = mesh->mFaces[i];
        indices.push_back(face.mIndices[0]);
        indices.push_back(face.mIndices[1]);
        indices.push_back(face.mIndices[2]);
    }

    if(first){
        Init(vertices, indices, uvs);
        lastMesh = this;
    }
    else{
        childMeshes.push_back(Mesh(vertices, indices, uvs));
        if(numChilds > 0){
            lastMesh = &childMeshes[lastMesh->GetChildMeshes().size()-1];
        }
    }
}

std::vector<Mesh>& Mesh::GetChildMeshes(){
    return childMeshes;
}

void Mesh::Init(std::vector<glm::vec3>& vertices, std::vector<uint>& indices, std::vector<glm::vec2>& uvs){
    indices_size = indices.size();
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // vertices    
    glGenBuffers(2, buffers);
    glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    
    // uvs
    glBindBuffer(GL_ARRAY_BUFFER, buffers[1]);
    glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, idx_buf);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(uint), &indices[0], GL_STATIC_DRAW);
    glBindVertexArray(0);
}

void Mesh::Render(){
    glBindVertexArray(vao);
    glDrawArrays(GL_TRIANGLES, 0, indices_size);
    glBindVertexArray(0);

    for(size_t i = 0; i < childMeshes.size(); i++){
        childMeshes[i].Render();
    }
}
