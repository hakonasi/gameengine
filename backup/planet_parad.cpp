#include <iostream>

class Planet{
public:
    Planet(){

    }
    Planet(std::string name, double orbitalPeriod,
            double aphelion, double perihelion){
        this->name = name;
        this->orbitalPeriod = orbitalPeriod;
        this->aphelion = aphelion;
        this->perihelion = perihelion;
    }
    ~Planet(){

    }

    std::string GetName(){
        return name;
    }
    double GetOrbitalPeriod(){
        return orbitalPeriod;
    }
    double GetAphelion(){
        return aphelion;
    }
    double GetPerihelion(){
        return perihelion;
    }
    double GetMass(){
        return mass;
    }
    double GetEccentricity(){
        return eccentricity;
    }

    void SetName(std::string name){
        this->name = name;
    }
    void SetOrbitalPeriod(double period){
        this->orbitalPeriod = period;
    }
    void SetAphelion(double aphelion){
        this->aphelion = aphelion;
    }
    void SetPerihelion(double perihelion){
        this->perihelion = perihelion;
    }
    void SetMass(double mass){
        this->mass = mass;
    }
    void SetEccentricity(double eccentricity){
        this->eccentricity = eccentricity;
    }
private:
    std::string name;
    double orbitalPeriod; // в днях
    double eccentricity;
    double aphelion;
    double perihelion;
    double mass;
};

int main(){
    Planet mars("Mars", 686.971, 249200000.0, 206700000.0);
    mars.SetEccentricity(0.0933941);
    return 0;
}
