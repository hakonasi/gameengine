#include <iostream>

unsigned int people;
float result = 1.0f;

int main(){
    std::cout << "Input a count of people: ";
    std::cin >> people;

    for(unsigned int i = 0; i < people; i++){
        result *= (float)(365-i)/365;
    }
    std::cout << result * 100 << "%" << std::endl;

    return 0;
}
