﻿#include "shader.h"

// type 0 = vertex shader
// type 1 = fragment shader
Shader::Shader(){
}

Shader::~Shader(){
    glDeleteShader(program);
}

bool Shader::LoadFile(const char* filename, std::string& m_out){
    std::ifstream in(filename);
    if(!in.is_open()){
        printf("Failed to open file %s\n", filename);
        return false;
    }

    std::string str;
    char tmp[300];
    while(!in.eof()){
        in.getline(tmp, 300);
        str+=tmp;
        str+='\n';
    }
    
    m_out = str;
    return true;
}

// vname = vertex shader
// fname = fragment shader
bool Shader::Init(const char* vname, const char* fname){
    std::string source;
    if(!LoadFile(vname, source))
        return false;
    
    vs = LoadShader(source, GL_VERTEX_SHADER);
    source = "";
    
    if(!LoadFile(fname, source))
        return false;
    fs = LoadShader(source, GL_FRAGMENT_SHADER);
  
    program = glCreateProgram();
    glAttachShader(program, vs);
    glAttachShader(program, fs);
    glLinkProgram(program);
    glDetachShader(program, vs);
    glDetachShader(program, fs);
    return true;
}

uint Shader::LoadShader(std::string& source, uint mode){
    uint id = glCreateShader(mode);

    const char* csource = source.c_str();

    glShaderSource(id, 1, &csource, NULL);
    glCompileShader(id);
    char error[1000];
    glGetShaderInfoLog(id, 1000, NULL, error);
    printf("Compiling shader: %s\n", error);
    return id;
}

uint Shader::GetProgramID(){
    return program;
}

void Shader::SetID(std::string uniformName){
    GLuint tmpID = glGetUniformLocation(program, uniformName.c_str());
    ids.push_back(std::make_pair(tmpID, uniformName.c_str()));
}

GLuint Shader::GetID(std::string uniformName){
    for(size_t i = 0; i < ids.size(); i++){
        if(ids[i].second == uniformName.c_str()){
            return ids[i].first;       
        }
    }
    return 0;
}

void Shader::Use(){
    glUseProgram(program);
}
