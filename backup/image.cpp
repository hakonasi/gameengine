#include "image.h"

Image::Image(){

}

Image::Image(const char* filename){
    LoadImage(filename); 
}

void Image::LoadImage(const char* filename){
    int width = 0;
    int height = 0;
    unsigned char* data;
    try{
        data = SOIL_load_image(filename, &width, &height, 0, SOIL_LOAD_RGB);
    }
    catch(const std::exception& ex){
        printf("%s\n", ex.what());
    }

    m_texture = CreateTexture(width, height, data);
    SOIL_free_image_data(data);
}

GLuint Image::CreateTexture(int width, int height, unsigned char* data){
    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);   
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    glBindTexture(GL_TEXTURE_2D, 0);
    return texture;
}

GLuint Image::GetTexture(){
    return m_texture;
}
