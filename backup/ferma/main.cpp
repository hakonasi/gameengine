#include <iostream>
#include <cmath>
#include <fstream>

#define BORD 1000000
#define POW 3

int main(){
    std::ofstream outs("out.txt");

    for(long long x = 1.0; x < BORD; x+=1.0){
        for(long long y = 1.0; y < BORD; y+=1.0){
            long long xPow = pow(x, POW);
            long long yPow = pow(y, POW);
            long long zPre = xPow + yPow;
            double z = pow(zPre, 1.0/POW);
            //            std::cout << "x " << x << " y " << y << " " <<  pow(x, POW) + pow(y, POW) << std::endl;
            if((double)round(z) == z){
                outs << x << " " << y << " " << z << " " << xPow << " " << yPow << " " << zPre << std::endl;
//                std::cout << x << " " << y << " " << z << std::endl;
                //return 0;           
            }
        }
    }
    return 0;
}
