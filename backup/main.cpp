// 04-01-2020 by Gordey Balaban

#include "window.h"
#include "shader.h"
#include "mesh.h"
#include "camera.h"
#include "input.h"
#include "time.h"
#include "entity.h"
#include "image.h"

Window window(800, 600, "C++ game engine", 3,3);
Shader shader;
Mesh mesh;
Camera camera;
Time m_time;
// пришлось input объявить через указатель,
// в противном случае callback не работает
// (т.к. отличаются адреса объектов)
Input* input;

Entity obj[2][2][2];

Image image;
Material material;
float x = 0.0f;

void render(){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.0f, 0.7f, 0.3f, 0.0f);

    m_time.Update();
    input->Update();
    
    camera.Yaw(input->GetDeltaX() * 10.0f);
    camera.Pitch(input->GetDeltaY() * -10.0f);
    camera.Movement();
    if(input->IsKeyPressed(GLFW_KEY_W))
        camera.Front(); 
    
    if(input->IsKeyPressed(GLFW_KEY_A))
        camera.Left(); 
    
    if(input->IsKeyPressed(GLFW_KEY_S))
        camera.Back(); 
    
    if(input->IsKeyPressed(GLFW_KEY_D))
        camera.Right(); 

    if(input->IsKeyPressed(GLFW_KEY_R))
        camera.Up();
    
    if(input->IsKeyPressed(GLFW_KEY_F))
        camera.Down();

    for(int a = 0; a < 2; a++){
        for(int b = 0; b < 2; b++){
            for(int c = 0; c < 2; c++){
                obj[a][b][c].GetMaterial().color.x = sin((x + a+b+c)*0.3);
                obj[a][b][c].GetMaterial().color.y = sin((x + a+b)*0.3);
                obj[a][b][c].GetMaterial().color.z = sin((x + b+c)*0.3);
                obj[a][b][c].Render(shader, &camera);
            }
        }
    }
    x += 10.0f * m_time.GetDeltaTime();
}

void exit(){
    window.Exit();
}

int main(){
    if(!window.Init()){
        return -1;
    }
    mesh.ImportMesh("cube.obj");
    shader.Init("vs.glsl", "fs.glsl");
    camera = Camera(glm::vec3(0.0f, 0.0f, 3.0f), 60.0f, 0.1f, 1000.0f, &m_time);
    camera.SetSpeed(8.0f);
    input = new Input(window.GetWindow());
    window.SetInput(input);
    window.SetCamera(&camera);

    input->AddKeyEvent(GLFW_KEY_ESCAPE, GLFW_PRESS, std::bind<void()>(exit));

    shader.SetID("mvp");
    shader.SetID("material.color");

    image = Image("image.png");
    material.color = glm::vec3(0.3f, 0.6f, 0.9f);    
    
    for(int a = 0; a < 2; a++){
        for(int b = 0; b < 2; b++){
            for(int c = 0; c < 2; c++){
                obj[a][b][c].SetMesh(&mesh);
                obj[a][b][c].SetTexture(&image);
                obj[a][b][c].SetMaterial(material);
                obj[a][b][c].SetPosition(glm::vec3(a*15,b*15,c*15));
            }
        }
    }

    window.Update(render);
   
    delete input;
    printf("Exiting from engine\n");
    return 0;
}
