#version 330 core

layout(location=0) in vec3 vertexPos;
layout(location=1) in vec2 uvs;

out vec2 uv;
uniform mat4 mvp;

void main(){
    uv = uvs;
    gl_Position = mvp * vec4(vertexPos, 1.0);
}
