#include <iostream>
#include <sol/sol.hpp>
#include <string>
#include <vector>

bool startsWith(const std::string &key, const std::string &prefix)
{
    return (key.size() >= prefix.size()) && (key.compare(0, prefix.size(), prefix) == 0);
}

int main(){
    static const std::string metaPrefix = "__";

    sol::state luaState;
    luaState.open_libraries(sol::lib::base);

    //luaState["a"] = 10;
    luaState.script_file("script.lua");
    
    return 0;
} 
