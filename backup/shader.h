#ifndef SHADER_H
#define SHADER_H

#include "window.h"
#include <fstream>
#include <algorithm>

class Shader{
    public:
        // type 0 = vertex shader
        // type 1 = fragment shader
        Shader();
        ~Shader();
        
        bool Init(const char* vname, const char* fname);
        void Use();
        void SetID(std::string uniformName);
        GLuint GetID(std::string uniformName);
        uint GetProgramID();
    private:
        bool LoadFile(const char* filename, std::string& m_out);
        uint LoadShader(std::string& source, uint mode);
        uint vs, fs, program;

        std::vector<std::pair<GLuint, std::string>> ids;
};

#endif
