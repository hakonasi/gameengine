#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "time.h"

class Time;

class Camera{
    public:
        Camera();
        Camera(glm::vec3 position, float fov, float near, float far, Time* m_time); 
        ~Camera();

        void Movement();
        void SetPosition(glm::vec3 position);
        glm::vec3& GetPosition();
    
        void Front();
        void Back();
        void Right();
        void Left();
        void Up();
        void Down();
        glm::mat4 GetView();
        glm::mat4 GetProjection();

        void SetSpeed(float speed);
        void UpdateProjection(int width, int height);

        void Yaw(float offset);
        void Pitch(float offset);
    private:
        glm::mat4 view;
        glm::vec3 position;
        glm::vec3 target;
        glm::vec3 direction;
        glm::mat4 projection;
        glm::vec3 right;
        glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
        glm::vec3 front = glm::vec3(0.0f, 0.0f, -1.0f);

        Time* m_time;
        float fov, near, far;
        float speed = 3.0f;
        float mouseSpeed = 1.2f;
        float yaw, pitch;
};

#endif
