#include <iostream>
#include <cmath>

double a;
double b;
double e;

int mode = 0;

double calcE(double a, double b){
    double result = sqrt(1-((b*b)/(a*a)));
    return result;
}

double calcB(double a, double e){
    double result = sqrt(-e*e*a*a+a*a);
    return result;
}

double calcApsis(double a, double e){
    double result = (1+e) * a;
    return result;
}

int main(){
    printf("Please, select a mode:\n");
    printf("0: search an [e]\n");
    printf("1: search a [b]\n");
    printf("2: calc apsis\n");

    do{
        std::cin >> mode;
    }
    while(mode < 0 || mode > 2);

    switch(mode){
        case 0:
            printf("Input [a], then [b]: \n");
            std::cin >> a >> b;
            if(b*b <= a*a)
                //std::cout << "Result is " << calcE(a, b) << std::endl;
                printf("Result is %.16f\n", calcE(a, b));
            else
                printf("[b^2] is not <= a^2\n");
            break;
        case 1:
            printf("Input [a], then [e]: \n");
            std::cin >> a >> e;
            if(e*e <= 1)
                //std::cout << "Result is " << calcB(a, e) << std::endl;
                printf("Result is %.16f\n", calcB(a, e));
            else
                printf("[e] is not <= 1\n");
            break;
        case 2:
            printf("Input [a], then [e]: \n");
            std::cin >> a >> e;
            printf("Result is %.16f\n", calcApsis(a, e));
            break;
    }
    return 0;
}
