#ifndef INPUT_H
#define INPUT_H

#include <vector>
#include <tuple>
#include <functional>
#include "window.h"

class Input{
public:
    Input();
    Input(GLFWwindow* window);
    ~Input();
    
    bool IsKeyPressed(int key);
    void AddKeyEvent(int key, int action, std::function<void()> func); 
    // обновить mouse delta
    void Update();
    float GetDeltaX();
    float GetDeltaY();
private:
    GLFWwindow* window;
    static void InputCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
    void KeyCallback(int key, int scancode, int action, int mods);
    // Очередь на обработку нажатий клавиш
    // int - клавиша, void* - функция, которая выполняется
    // при нажатии на клавишу
    std::vector< std::tuple<int, int, std::function<void()> >> keyEvents;
    float deltaX = 0;
    float deltaY = 0;
    double lastX, lastY;
};

#endif
