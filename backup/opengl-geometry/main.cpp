#include "shader.h"

GLuint VAO;
GLuint buffers[2];
// VBO is buffer

float vertices[] = {
-1.0f, 1.0f, 0.0f,
0.0f, -1.0f, 0.0f,
1.0f, 1.0f, 0.0f
};

GLuint indices[] = {
0,1,2
};

void Draw(){
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    
    glGenBuffers(2, buffers);
    glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
    glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(float), vertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);


    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * sizeof(GLuint), indices, GL_STATIC_DRAW);

    glBindVertexArray(0);
}

Shader m_shader;

int main(){
    if(!glfwInit()){
        std::cout << "Failed to init GLFW. Check your suppored version of OpenGL using glxinfo." << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    GLFWwindow* window = glfwCreateWindow(640, 480, "Window", NULL, NULL);
    if(!window){
        std::cout << "Failed to create a window." << std::endl;
        glfwTerminate();
        return -2;
    }
   
    glfwMakeContextCurrent(window);
    
    glewExperimental = GL_TRUE;
    if(glewInit() != GLEW_OK){
        std::cout << "Failed to init GLEW." << std::endl;
        return -3;
    }

    m_shader.InitShader("vertex.glsl", "fragment.glsl");

    while(!glfwWindowShouldClose(window)){
        glClear(GL_COLOR_BUFFER_BIT);
        glClearColor(0.0f, 0.7f, 0.9f, 1.0f);

        glUseProgram(m_shader.GetProgramID());
        glBindVertexArray(VAO);
        glDrawElements( GL_TRIANGLES, 3,  GL_UNSIGNED_INT, indices);

        //glDrawArrays(GL_LINE_STRIP, 0, 4);
        glBindVertexArray(0);

        if(glfwGetKey(window, GLFW_KEY_ESCAPE)){
            glfwTerminate();
            return 0;
        }

        glfwPollEvents();
        glfwSwapBuffers(window);
    }

    glfwTerminate();

    return 0;
}
