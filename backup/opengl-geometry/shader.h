#ifndef Shader_H
#define Shader_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <cassert>
#include <map>
#include <fstream>

class Shader
{
public:
    Shader();
    ~Shader();

    void LoadFile(const char* filename, std::string& str);
    GLuint LoadShader(std::string& source, GLuint  mode);
    void InitShader(const char* vname, const char* fname);
    void Clean();

    GLuint GetProgramID();
private:
    GLuint vs, fs, program;

};

#endif
