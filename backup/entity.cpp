#include "entity.h"

Entity::Entity(){

}

Entity::Entity(Mesh* mesh){
    this->mesh = mesh;
}

Entity::~Entity(){

}

void Entity::SetMaterial(Material material){
    this->material = material;
}

Material& Entity::GetMaterial(){
    return material;
}

void Entity::SetPosition(glm::vec3 position){
    this->position = position;
}

void Entity::SetMesh(Mesh* mesh){
    this->mesh = mesh;
}

void Entity::SetTexture(Image* image){
    this->image = image;
}

Mesh* Entity::GetMesh(){
    return mesh;
}

void Entity::Render(Shader& shader, Camera* camera){
    shader.Use();
    model = glm::translate(glm::mat4(1.0f), position);


    glm::mat4 mvp = camera->GetProjection() * camera->GetView() * model;
    glUniformMatrix4fv(shader.GetID("mvp"), 1, GL_FALSE, glm::value_ptr(mvp));
   
    glUniform3fv(shader.GetID("material.color"), 1, glm::value_ptr(material.color));
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, image->GetTexture());
    mesh->Render();
}
