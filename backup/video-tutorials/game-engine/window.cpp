#include "window.h"

Window::Window(){

}

Window::Window(int width, int height, std::string name, int major, int minor){
    Init(width, height, name, major, minor);
}

Window::~Window(){
    glfwTerminate();
}

void Window::SetInput(Input* input){
    this->input = input;
}

Input* Window::GetInput(){
    return input;
}

GLFWwindow* Window::GetWindow(){
    return m_window;
}

void Window::Update(void(*function)()){
    while(!glfwWindowShouldClose(m_window)){
        function();

        glfwSwapBuffers(m_window);
        glfwPollEvents();
    }
}

bool Window::Init(int width, int height, std::string name, int major, int minor){
    if(!glfwInit()){
        std::cout << "Failed to init GLFW" << std::endl;
        return false;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, major);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, minor);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    m_window = glfwCreateWindow(width, height, name.c_str(), NULL, NULL);

    if(!m_window){
        std::cout << "Failed to create a window" << std::endl;
        glfwTerminate();
        return false;
    }

    glfwMakeContextCurrent(m_window);
    glfwSetWindowUserPointer(m_window, this);

    glewExperimental = true;
    if(glewInit() != GLEW_OK){
        std::cout << "Failed to init GLEW" << std::endl;
        glfwTerminate();
        return false;
    }
    return true;
}

