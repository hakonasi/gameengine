#ifndef INPUT_H
#define INPUT_H

#include <vector>
#include <tuple>
#include <functional>

#include "window.h"

class Window;

class Input{
public:
    Input();
    Input(Window* window);
    ~Input();

    bool IsKeyPressed(int key);
    void AddEvent(int key, int action, std::function<void()> function);
private:
    static void CallbackStatic(GLFWwindow* window, int key, int scancode, int action, int mods);
    void Callback(int key, int scancode, int action, int mods);

    std::vector<std::tuple<int, int, std::function<void()>>> events;

    Window* window;
};

#endif
