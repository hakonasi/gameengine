#include "window.h"
#include "input.h"

Window window(800, 600, "C++ game engine", 3,3);
Input* input;

void rendering(){
    glClear(GL_COLOR_BUFFER_BIT);
    glClearColor(0.0f, 0.7f, 0.9f, 1.0f);

    if(input->IsKeyPressed(GLFW_KEY_U)){
        std::cout << "u was pressed" << std::endl;
    }
}

void foo(){
    std::cout << "foo is working!" << std::endl;
}

int main(){
    input = new Input(&window);
    window.SetInput(input);

    input->AddEvent(GLFW_KEY_T, GLFW_RELEASE, foo);
    window.Update(rendering); 
    
    delete input;
    return 0;
}
