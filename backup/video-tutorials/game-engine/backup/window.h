#ifndef WINDOW_H
#define WINDOW_H

#include <iostream>
#include <string>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "input.h"

class Input;

class Window{
public:
    Window();
    Window(int width, int height, std::string name, int major, int minor);
    ~Window();

    GLFWwindow* GetWindow();

    void SetInput(Input* input);
    Input* GetInput();
    bool Init(int width, int height, std::string name, int major, int minor);
    void Update(void(*function)());
private:
    GLFWwindow* m_window;
    Input* input;
};

#endif
