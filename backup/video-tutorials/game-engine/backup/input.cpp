#include "input.h"

Input::Input(){
    
}

Input::Input(Window* window){
    this->window = window;
    glfwSetKeyCallback(window->GetWindow(), CallbackStatic);
}

Input::~Input(){

}

bool Input::IsKeyPressed(int key){
    return glfwGetKey(window->GetWindow(), key) == GLFW_PRESS;
}

void Input::AddEvent(int key, int action, std::function<void()> function){
    events.push_back(std::make_tuple(key, action, function));
}

void Input::Callback(int key, int scancode, int action, int mods){
    for(size_t i = 0; i < events.size(); i++){
        if(key == std::get<0>(events[i]) && action == std::get<1>(events[i])){
            std::get<2>(events[i])();
        }
    }
}

void Input::CallbackStatic(GLFWwindow* window, int key, int scancode, int action, int mods){
    Window* obj = static_cast<Window*>(glfwGetWindowUserPointer(window));
    Input* m_input = obj->GetInput();
    m_input->Callback(key, scancode, action, mods);
}
