#include "common.h"

#define COUNT 1000

std::vector<int> simpleNums(1);

int main(){
    int a,b;
    std::cin >> a >> b;
   
    simpleNums[0] = 2;
    bool f = true;

    for(int i = 3; i < COUNT; i++){
        f = true;
        for(int j = 0; j < simpleNums.size(); j++){
            if(i % simpleNums[j] == 0 && simpleNums[j] != i){
                f = false;
                break;
            }
        }
        if(f)
            simpleNums.push_back(i);
    }
    //veccout(simpleNums);    
    return 0;
}
