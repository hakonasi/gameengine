#include <iostream>
#include <cstdlib>

void coutArr(int* t){
    for(int i = 0; i < sizeof(t)/sizeof(t[0]); i++){
        std::cout << t[i] << " ";
    }
    std::cout << std::endl;
}

unsigned int fact(int x){
    unsigned int j = 1;
    for(unsigned int i = 1; i <= x; i++){
        j*=i;
    }
    return j;
}

int main(){
    int n;
    std::cin >> n;
    
    int* a = (int*)malloc(n);
    std::cout << "size is: " << sizeof(a)/sizeof(a[0]) << std::endl;
    unsigned int count = fact(n);
    
    for(int i = 1; i <= n; i++){
        a[i-1] = i;
    }
    int j;
    int l;
    for(int i = 0; i < count; i++){
        j = 0;
        l = 0;
        for(int q = 0; q < n; q++){
            if(a[q] < a[q+1] && q > j && q < n){
                j = q;
            }
        }
        for(int q = 0; q < n; q++){
            if(q > j && a[q] > a[j]){
                l = q;
            }
        }
        int tmp_aj = a[j];
        a[j] = a[l];
        a[l] = tmp_aj;

        coutArr(a);
    }    
    free(a);
    return 0;
}
