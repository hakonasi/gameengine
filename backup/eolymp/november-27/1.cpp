#include <iostream>
#include <vector>

int t;

unsigned long long fact(unsigned long long x){
    unsigned long long n = 1;
    for(unsigned long long i = 1; i <= x; i++){
        n *= i;
    }
    return n;
}

int main(){
    std::cin >> t;
    std::vector<unsigned long long> n(t);
    std::vector<unsigned long long> k(t);
    for(int i = 0; i < t; i++){
        std::cin >> n[i] >> k[i];
    }
    for(int i = 0; i < t; i++){
        std::cout << fact(n[i])/(fact(k[i])*fact(n[i]-k[i])) << std::endl;
    }
    return 0;
}
