#include <iostream>
#include <cmath>

int fact(int x){
    int n = 1;
    for(int i = 1; i <= x; i++){
        n*=i;
    }
    return n;
}

int main(){
    int m,n;

    std::cin >> n >> m; 
    int x = 0;
    x = fact(n)/((fact(n-m)*(fact(m))));
    std::cout << x << std::endl;    
    return 0;
}
