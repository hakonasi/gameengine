#include "common.h"

#define COUNT 100

std::vector<int> simpleNums(1);
std::vector<int> result;

int main(){
    int a;
    std::cin >> a;
   
    simpleNums[0] = 2;
    bool f = true;

    for(int i = 3; i < COUNT; i++){
        f = true;
        for(int j = 0; j < simpleNums.size(); j++){
            if(i % simpleNums[j] == 0 && simpleNums[j] != i){
                f = false;
                break;
            }
        }
        if(f)
            simpleNums.push_back(i);
    }

    for(int i = 0; i < simpleNums.size(); i++)
    {
        while(a % simpleNums[i] == 0){
            a /= simpleNums[i];
            result.push_back(simpleNums[i]);
        }
    }
    for(int i = 0; i < result.size(); i++)
        std::cout << result[i] << " ";
    std::cout << std::endl;
    //veccout(simpleNums);    
    return 0;
}
