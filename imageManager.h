#ifndef IMAGE_MANAGER_H
#define IMAGE_MANAGER_H

#include "image.h"

class ImageManager{
public:
    ImageManager();
    ~ImageManager();

    std::vector<Image>& GetImages();
    std::vector<Image>& GetGizmos();
    void AddImage(Image* image, bool isGizmo=false);
    bool AddImage(std::string path, bool isGizmo=false);
    void Reset();
    Image* GetImage(size_t index);
private:
    std::vector<Image> m_images;
    std::vector<Image> m_gizmos;
};

#endif
