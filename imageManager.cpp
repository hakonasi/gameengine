#include "imageManager.h"

ImageManager::ImageManager(){

}

ImageManager::~ImageManager(){

}

void ImageManager::AddImage(Image* image, bool isGizmo){
    if(!isGizmo)
        m_images.push_back(*image);
    else
        m_gizmos.push_back(*image);
}

bool ImageManager::AddImage(std::string path, bool isGizmo){
    Image img(path.c_str());
    if(!isGizmo)
        m_images.push_back(img);
    else
        m_gizmos.push_back(img);
    return true;
}

void ImageManager::Reset(){
    m_images.clear();
    Image::indicesCounter = 0;
}

Image* ImageManager::GetImage(size_t index){
    for(auto& i : m_images){
        if(i.GetIndex() == index){
            return &i;
        }
    }
    assert(0 == 1);
}

std::vector<Image>& ImageManager::GetImages(){
    return m_images;
}

std::vector<Image>& ImageManager::GetGizmos(){
    return m_gizmos;
}
