#include "input.h"

Input::Input(){

}

Input::Input(GLFWwindow* window, Camera* camera){
    this->window = window;
    this->camera = camera;
    glfwSetKeyCallback(window, InputCallback);
}

Input::~Input(){

}

void Input::AddKeyEvent(int key, int action, std::function<void()> func){
    keyEvents.push_back(std::make_tuple(key, action, func));
}

void Input::KeyCallback(int key, int scancode, int action, int mods){
    if(keyEvents.size() > 0){
        for(size_t i = 0; i < keyEvents.size(); i++){
            if(key == std::get<0>(keyEvents[i]) && action == std::get<1>(keyEvents[i])){
                std::get<2>(keyEvents[i])();
            }
        }
    }
}

bool Input::IsKeyPressed(int key){
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool Input::IsMousePressed(int key){
    return glfwGetMouseButton(window, key) == GLFW_PRESS;
}

float Input::GetDeltaX(){
    return deltaX;
}

float Input::GetDeltaY(){
    return deltaY;
}

void Input::EngineInput(){
    if(Logic::is_engine_input){
        if(IsMousePressed(GLFW_MOUSE_BUTTON_RIGHT)){
            camera->Yaw(deltaX * 10.0f);
            camera->Pitch(deltaY * -10.0f);
        }
        if(IsKeyPressed(GLFW_KEY_W))
            camera->Front(); 
        
        if(IsKeyPressed(GLFW_KEY_A))
            camera->Left(); 
        
        if(IsKeyPressed(GLFW_KEY_S))
            camera->Back(); 
        
        if(IsKeyPressed(GLFW_KEY_D))
            camera->Right(); 

        if(IsKeyPressed(GLFW_KEY_E))
            camera->Up();
        
        if(IsKeyPressed(GLFW_KEY_Q))
            camera->Down();
        
        camera->Movement();
    }
}

void Input::Update(){
    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);
    deltaX = xpos - lastX;
    deltaY = ypos - lastY;
    lastX = xpos;
    lastY = ypos;
    /*printf("%f\n", deltaX);

    int wxpos, wypos;
    int width, height;
    glfwGetWindowSize(window, &width, &height);
    glfwGetWindowPos(window, &wxpos, &wypos);
    if(xpos < wxpos + 2){
        glfwSetCursorPos(window, wxpos + width - 3, ypos);
        lastX = -10;
    }
    else if(xpos > wxpos + width - 2){
        glfwSetCursorPos(window, wxpos + 3, ypos);
        deltaX = 0;
        lastX = 0;
    }*/
}

void Input::InputCallback(GLFWwindow* window, int key, int scancode, int action, int mods){
    Window* obj = static_cast<Window*>(glfwGetWindowUserPointer(window));
    obj->GetInput()->KeyCallback(key, scancode, action, mods); 
}
