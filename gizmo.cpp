#include "gizmo.h"

Gizmo::Gizmo(){
    type = 0;
}

Gizmo::Gizmo(ImageManager* imageManager, int type){
    this->type = type;
    this->imageManager = imageManager;
    switch(type){
        case 0:
            texture = imageManager->GetGizmos()[0].GetTexture();
            break;
    }
}

Gizmo::~Gizmo(){

}

void Gizmo::Render(Shader& shader, Camera* camera){
    shader.Use();
    model = glm::translate(glm::mat4(1.0f), position);
    glUniform3fv(shader.GetID("camPos"), 1, glm::value_ptr(camera->GetPosition()));
    glUniformMatrix4fv(shader.GetID("model"), 1, GL_FALSE, glm::value_ptr(model));
    glUniformMatrix4fv(shader.GetID("view"), 1, GL_FALSE, glm::value_ptr(camera->GetView()));
    glUniformMatrix4fv(shader.GetID("proj"), 1, GL_FALSE, glm::value_ptr(camera->GetProjection()));
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);

//    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);

    glBindVertexArray(BasicQuad::GetVAO());
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);

    glEnable(GL_DEPTH_TEST);
//    glEnable(GL_CULL_FACE);
}

void Gizmo::SetPosition(glm::vec3 position){
    this->position = position;
}

glm::vec3& Gizmo::GetPosition(){
    return position;
}
