﻿#include "shader.h"

// type 0 = vertex shader
// type 1 = fragment shader
Shader::Shader(){
}

Shader::~Shader(){
    glDeleteShader(program);
}

// vname = vertex shader
// fname = fragment shader
bool Shader::Init(const char* vname, const char* fname){
    std::string vertexSource;
    std::string fragmentSource;

    std::ifstream vertexIn;
    std::ifstream fragmentIn;

    vertexIn.exceptions(std::ifstream::badbit | std::ifstream::failbit);
    fragmentIn.exceptions(std::ifstream::badbit | std::ifstream::failbit);
    try{
        vertexIn.open(vname);
        fragmentIn.open(fname);

        std::stringstream vertexSS;
        std::stringstream fragmentSS;
        
        vertexSS << vertexIn.rdbuf();
        fragmentSS << fragmentIn.rdbuf();

        vertexSource = vertexSS.str();
        fragmentSource = fragmentSS.str();

        std::string tmp = vertexSS.str();
    }
    catch(std::ios_base::failure e){
        printf("Failed ot load file %s\n", e.what());
    }

    GLuint vs = LoadShader(vertexSource, GL_VERTEX_SHADER);
    GLuint fs = LoadShader(fragmentSource, GL_FRAGMENT_SHADER);

    program = glCreateProgram();
    glAttachShader(program, vs);
    glAttachShader(program, fs);
    glLinkProgram(program);
    glDetachShader(program, vs);
    glDetachShader(program, fs);
    return true;
}

uint Shader::LoadShader(std::string& source, uint mode){
    uint id = glCreateShader(mode);

    const char* csource = source.c_str();

    glShaderSource(id, 1, &csource, NULL);
    glCompileShader(id);
    char error[1000];
    glGetShaderInfoLog(id, 1000, NULL, error);
    printf("Compiling shader: %s\n", error);
    return id;
}

uint Shader::GetProgramID(){
    return program;
}

void Shader::SetID(std::string uniformName){
    GLuint tmpID = glGetUniformLocation(program, uniformName.c_str());
    ids.push_back(std::make_pair(tmpID, uniformName.c_str()));
}

GLuint Shader::GetID(std::string uniformName){
    for(size_t i = 0; i < ids.size(); i++){
        if(ids[i].second == uniformName.c_str()){
            return ids[i].first;       
        }
    }
    return 0;
}

void Shader::Use(){
    glUseProgram(program);
}
