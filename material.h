#ifndef MATERIAL_H
#define MATERIAL_H

#include "window.h"

struct Material{
    glm::vec3 albedo_col = glm::vec3(1.0f, 1.0f, 1.0f);
    Image* albedo_tex = nullptr;

    float metallic = 0.0f;
    float roughness = 0.0f;
    float ao = 0.0f;
};

#endif
