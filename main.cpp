// 04-01-2020 by Gordey Balaban

#include "window.h"
#include "shader.h"
#include "mesh.h"
#include "camera.h"
#include "input.h"
#include "time.h"
#include "entity.h"
#include "image.h"
#include "imageManager.h"
#include "gui.h"
#include "entityManager.h"
#include "common/meshes.h"
#include <ctime>

#define IMGUI_IMPL_OPENGL_LOADER_GLEW

Window window(800, 600, "C++ game engine", 3,3);
Shader shader, gizmo_shader;
Mesh mesh;
Camera camera;
Time m_time;
// пришлось input объявить через указатель,
// в противном случае callback не работает
// (т.к. отличаются адреса объектов)
Input* input;
Gui gui;
Material material;
MeshManager meshManager;
EntityManager entityManager;
ImageManager imageManager;

SceneLoader sceneLoader;

// basic meshes
BasicQuad m_basicQuad;

void render(){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.0f, 0.7f, 0.3f, 0.0f);

    m_time.Update();
    input->Update();    
    input->EngineInput();

    for(size_t i = 0; i < entityManager.GetEntities().size(); i++){
        entityManager.GetEntities()[i].Render(shader, gizmo_shader, &camera);
    }

    gui.Loop();
}

void exit(){
    window.Exit();
}

int main(){
    srand(std::time(0));
    if(!window.Init()){
        return -1;
    }
//    meshes.push_back(Mesh("antoshka.obj"));
//    meshes.push_back(Mesh("truba.obj"));
    shader.Init("vs.glsl", "fs.glsl");
    gizmo_shader.Init("gizmo_vs.glsl", "gizmo_fs.glsl");
    camera = Camera(glm::vec3(0.0f, 0.0f, -3.0f), 60.0f, 0.1f, 1000.0f, &m_time);
    camera.SetSpeed(15.0f);
    input = new Input(window.GetWindow(), &camera);
    window.SetInput(input);
    window.SetCamera(&camera);
    sceneLoader = SceneLoader(&imageManager, &meshManager, &entityManager, &gui);

    input->AddKeyEvent(GLFW_KEY_ESCAPE, GLFW_PRESS, std::bind<void()>(exit));

    m_basicQuad.InitQuad();

    // Adding a gizmo icon (for light).
    imageManager.AddImage("gizmo/light.png", true);

    shader.SetID("mvp");
    shader.SetID("modelMatrix");
    shader.SetID("camPos");
    shader.SetID("material.albedo_col");
    shader.SetID("material.albedo_tex");
    shader.SetID("material.metallic");
    shader.SetID("material.roughness");
    shader.SetID("material.ao");

    shader.SetID("light.position");
    shader.SetID("light.color");
    shader.SetID("light.range");
    shader.SetID("light.intensity");

    gizmo_shader.SetID("camPos");
    gizmo_shader.SetID("model");
    gizmo_shader.SetID("view");
    gizmo_shader.SetID("proj");
    gui.Init(window, &camera, &meshManager, &entityManager, &sceneLoader, &imageManager);

    //sceneLoader.SaveScene("scene.json", meshes, entities, images);
    //sceneLoader.LoadScene("scene.json", &meshManager, &entityManager, &imageManager);
    window.Update(render);
  
    mesh.Clean();
    m_basicQuad.Clean();
    delete input;
    printf("Exiting from engine\n");
    return 0;
}
