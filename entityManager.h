#ifndef ENTITY_MANAGER
#define ENTITY_MANAGER

#include "entity.h"

class EntityManager{
public:
    EntityManager();
    ~EntityManager();

    void AddEntity(Entity entity);
    void DeleteEntity(Entity* entity);

    void Reset();

    std::vector<Entity>& GetEntities();
private:
    std::vector<Entity> entities;  
};

#endif
