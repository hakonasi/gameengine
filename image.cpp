#include "image.h"

size_t Image::indicesCounter = 0;

Image::Image(){

}

Image::Image(const char* filename){
    LoadImage(filename); 
    this->index = indicesCounter;
    indicesCounter++;
}

size_t Image::GetIndex(){
    return index;
}

int Image::GetWidth(){
    return m_width;
}

int Image::GetHeight(){
    return m_height;
}

void Image::LoadImage(const char* filename){
    this->path = filename;
    int width = 0;
    int height = 0;
    unsigned char* data;
    data = SOIL_load_image(filename, &width, &height, 0, SOIL_LOAD_RGB);

    assert(data != NULL);

    m_texture = CreateTexture(width, height, data);
    SOIL_free_image_data(data);
}

GLuint Image::CreateTexture(int width, int height, unsigned char* data){
    m_width = width;
    m_height = height;
    
    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

    GLfloat fLargest;
    glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &fLargest);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, fLargest);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);   
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glGenerateMipmap(GL_TEXTURE_2D);
    
    glBindTexture(GL_TEXTURE_2D, 0);
    return texture;
}

std::string Image::GetPath(){
    return path;
}

GLuint Image::GetTexture(){
    return m_texture;
}
