#include "mesh.h"

size_t Mesh::indicesCounter = 0;

Mesh::Mesh(){

}

Mesh::Mesh(std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& normals, std::vector<uint>& indices, std::vector<glm::vec2>& uvs){
    indices_size = indices.size();
    this->index = indicesCounter;
    std::cout << "index: " <<  this->index << std::endl;
    indicesCounter++;
    Init(vertices, normals, indices, uvs);
}

Mesh::~Mesh(){
}

std::string Mesh::GetName(){
    return name;
}

void Mesh::SetName(std::string name){
    this->name = name;
}

std::string Mesh::GetPath(){
    return path;
}

void Mesh::SetPath(std::string path){
    this->path = path; 
}

std::vector<Mesh>& Mesh::GetChildMeshes(){
    return childMeshes;
}

size_t Mesh::GetIndex(){
    return index;
}

void Mesh::Clean(){
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(3, buffers);
}

void Mesh::Init(std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& normals, std::vector<uint>& indices, std::vector<glm::vec2>& uvs){
    indices_size = indices.size();
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // vertices    
    glGenBuffers(3, buffers);
    glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    
    // normals
    glBindBuffer(GL_ARRAY_BUFFER, buffers[1]);
    glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

    // uvs
    glBindBuffer(GL_ARRAY_BUFFER, buffers[2]);
    glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, idx_buf);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(uint), &indices[0], GL_STATIC_DRAW);
    glBindVertexArray(0);
}

void Mesh::Render(){
    glBindVertexArray(vao);
    glDrawArrays(GL_TRIANGLES, 0, indices_size);
    glBindVertexArray(0);

/*    for(size_t i = 0; i < childMeshes.size(); i++){
        childMeshes[i].Render();
    }*/
}
