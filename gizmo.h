#ifndef GIZMO_H
#define GIZMO_H

#include "window.h"
#include "imageManager.h"
#include "shader.h"
#include "camera.h"
#include "common/meshes.h"

class Gizmo{
public:
    Gizmo();
    Gizmo(ImageManager* imageManager, int type);
    ~Gizmo();

    glm::vec3& GetPosition();
    void SetPosition(glm::vec3 position);
    void Render(Shader& shader, Camera* camera);
private:
    glm::vec3 position;
    glm::mat4 model;
    ImageManager* imageManager;

    int type;
    GLuint texture;
};

#endif
