#include "meshes.h"

GLuint BasicQuad::vao = 0;

BasicQuad::BasicQuad(){
    
}

BasicQuad::~BasicQuad(){
    
}

void BasicQuad::InitQuad(){
    GLfloat vertices[] = {
        -1.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 0.0f,
        1.0f, -1.0f, 0.0f,
        1.0f, 1.0f, 0.0f
    };
    GLfloat uvs[] = {0.0f, 0.0f,
                    1.0f, 0.0f,
                    1.0f, 1.0f,
                    0.0f, 1.0f};

    GLushort indices[] = {0,1,2,0,2,3};

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // vertices    
    glGenBuffers(2, buffers);
    glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
    glBufferData(GL_ARRAY_BUFFER, 12 * sizeof(GLfloat), vertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

    // uvs
    glBindBuffer(GL_ARRAY_BUFFER, buffers[1]);
    glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(GLfloat), uvs, GL_STATIC_DRAW);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buf);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(GLushort), indices, GL_STATIC_DRAW);
    glBindVertexArray(0);
}

void BasicQuad::Clean(){
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(2, buffers);
}

GLuint BasicQuad::GetVAO(){
    return vao;
}
