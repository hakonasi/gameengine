#ifndef MISC_MESHES_H
#define MISC_MESHES_H

#include "../window.h"

class BasicQuad{
public:
    BasicQuad();
    ~BasicQuad();

    void InitQuad();
    void Clean();
    static GLuint GetVAO();
private:
    static GLuint vao;
    GLuint buffers[2];
    GLuint index_buf;
};

#endif
