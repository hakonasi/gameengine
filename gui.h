#ifndef GUI_H
#define GUI_H

#include "window.h"
#include "meshManager.h"
#include "logic.h"
#include "entityManager.h"
#include "imageManager.h"
#include "sceneLoader.h"

#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/imgui_impl_opengl3.h>

#include <ImGuiFileDialog/ImGuiFileDialog.h>

class Gui{
public:
    Gui();
    ~Gui();

    void Init(Window& window, Camera* camera, MeshManager* meshManager, EntityManager* entityManager, SceneLoader* sceneLoader, ImageManager* imageManager);
    void Loop();
    void SelectEntity(Entity* entity);
private:
    Camera* camera;
    MeshManager* meshManager;
    EntityManager* entityManager;
    ImageManager* imageManager;
    SceneLoader* sceneLoader;
    ImGuiIO* io;

    int last_light_type;

    // Properties of this entity are visible in the inspector window.
    Entity* m_selectedEntity;
    void MainMenu();
    void ToolsPanel();
    void AssetViewer();
    void ShowEntities(Entity& entity);
    void Inspector();
};

#endif
