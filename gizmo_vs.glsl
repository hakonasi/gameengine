#version 330 core

layout(location=0) in vec3 vertexPos;
layout(location=1) in vec2 uvs;

out vec2 uv;
uniform vec3 camPos;
uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main(){
    uv = uvs;
    gl_Position = proj * view * model * vec4(vertexPos, 1.0);
}
