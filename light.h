#ifndef LIGHT_H
#define LIGHT_H

#include "window.h"

struct Light{
    glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 color = glm::vec3(1.0f, 1.0f, 1.0f);

    float range = 1.0f;
    float intensity = 1.0f;
};

#endif
