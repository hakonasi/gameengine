#include "gui.h"

Gui::Gui(){
}

Gui::~Gui(){

}

void Gui::Init(Window& window, Camera* camera, MeshManager* meshManager, EntityManager* entityManager, SceneLoader* sceneLoader, ImageManager* imageManager){
    this->camera = camera;
    this->meshManager = meshManager;
    this->entityManager = entityManager;
    this->sceneLoader = sceneLoader;
    this->imageManager = imageManager;
    ImGui::CreateContext();
    io = &ImGui::GetIO(); (void)*io;

    ImGui::StyleColorsDark();

    ImGui_ImplGlfw_InitForOpenGL(window.GetWindow(), true);
    ImGui_ImplOpenGL3_Init("#version 330 core");
}

void Gui::SelectEntity(Entity* entity){
    assert(entity != nullptr);
    m_selectedEntity = entity;
}

void Gui::Loop(){
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    MainMenu();
    ToolsPanel(); 
    AssetViewer();
    Inspector();

    std::vector<Entity>& entities = entityManager->GetEntities();
    ImGui::Begin("Entities");
    for(auto& i : entities){
        ShowEntities(i);
    }
    ImGui::End();

    ImGui::Begin("Debug");
    ImGui::Text("Average %.3f ms/frame (%.1f FPS)", 1000.0f / io->Framerate, io->Framerate);

    float* position = const_cast<float*>(glm::value_ptr(camera->GetPosition()));
    ImGui::InputFloat3("Position", position, "%.3f", ImGuiInputTextFlags_ReadOnly);

    ImGui::End();

    if(ImGuiFileDialog::Instance()->Display("ImportMeshFileDialog")){
        if(ImGuiFileDialog::Instance()->IsOk() == true){
            std::string filePathName = ImGuiFileDialog::Instance()->GetFilePathName();
            std::string filePath = ImGuiFileDialog::Instance()->GetCurrentPath();
            meshManager->AddMesh(filePathName);
        }

        // close
        ImGuiFileDialog::Instance()->Close();
    }

    if(ImGuiFileDialog::Instance()->Display("ImportImageFileDialog")){
        if(ImGuiFileDialog::Instance()->IsOk() == true){
            std::string filePathName = ImGuiFileDialog::Instance()->GetFilePathName();
            std::string filePath = ImGuiFileDialog::Instance()->GetCurrentPath();
            imageManager->AddImage(filePathName);
        }

        // close
        ImGuiFileDialog::Instance()->Close();
    }
    ImGui::ShowDemoWindow();

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

void Gui::ShowEntities(Entity& entity){
    ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_AllowItemOverlap;
    bool hasChild = (entity.GetChilds().size() != 0);
    if(!hasChild)
        node_flags |= ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen;
    else
        node_flags |= ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick;

//    char buffer[] = {"abcdef\n2aa\n3aa"};
    bool m_tree = ImGui::TreeNodeEx(entity.GetName().c_str(), node_flags);
    if(ImGui::IsItemClicked()){
        m_selectedEntity = &entity;
    }
    //    bool m_tree = ImGui::TreeNodeEx(buffer, node_flags);

    if(hasChild){
        if(m_tree){
            for(auto& i : entity.GetChilds()){
                ShowEntities(i);
            }
            ImGui::TreePop();
        }
    }
}

void Gui::Inspector(){
    ImGui::Begin("Inspector");

    if(m_selectedEntity != nullptr){
        ImGui::Text(m_selectedEntity->GetName().c_str());
        ImGui::Text("ID: ");
        ImGui::SameLine();
        ImGui::Text(std::to_string(m_selectedEntity->GetIndex()).c_str());
        
        ImGui::Text("Type: ");
        ImGui::SameLine();
        int m_type = m_selectedEntity->GetType();
        switch(m_type){
            case 0:
                ImGui::Text("Mesh");
                break;
            case 1: case 2:
                ImGui::Text("Light");
                break;
        }
        
        if(ImGui::Button("Delete")){
            entityManager->DeleteEntity(m_selectedEntity);                 
        }
        if(ImGui::CollapsingHeader("Transform")){
            ImGui::DragFloat3("Position", glm::value_ptr(m_selectedEntity->GetPosition()), 0.1f); 
            ImGui::DragFloat3("Rotation", glm::value_ptr(m_selectedEntity->GetRotation()), 0.1f); 
            ImGui::DragFloat3("Scale", glm::value_ptr(m_selectedEntity->GetScale()), 0.1f); 
        }
        switch(m_type){
            case 0:{
                if(ImGui::CollapsingHeader("Material")){
                    ImGui::ColorEdit3("Albedo", glm::value_ptr(m_selectedEntity->GetMaterial().albedo_col));
               
                    Material* m_mat = &m_selectedEntity->GetMaterial();

                    if(m_mat->albedo_tex == nullptr){
                        ImGui::Button("No image", ImVec2(68, 68));
                    }
                    else{
                        Image* albedoTex = m_mat->albedo_tex;
                        ImTextureID albedoTex_id = (void*)albedoTex->GetIndex();
                        ImGui::ImageButton(albedoTex_id, ImVec2(64, 64), ImVec2(0, 0), 
                                    ImVec2(64.0f/albedoTex->GetWidth(),64.0f/albedoTex->GetHeight()), 2, ImVec4(0.0f, 0.0f, 0.0f, 1.0f));
                    }

                    if(ImGui::BeginDragDropTarget()){
                        if(const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("img_to_mat")){
                            assert(payload != nullptr);
                            size_t imgIndex = *(size_t*)payload->Data;
                            m_mat->albedo_tex = imageManager->GetImage(imgIndex);
                        }
                        ImGui::EndDragDropTarget();
                    }

                    ImGui::SameLine();
                    ImGui::BeginGroup();
                    ImGui::Text("Albedo texture");
                    if(ImGui::Button("Set to null")){
                        m_mat->albedo_tex = nullptr;
                    }
                    ImGui::EndGroup();

                    ImGui::DragFloat("Metallic", &m_mat->metallic, 0.01f, 0.0f, 1.0f);
                    ImGui::DragFloat("Roughness", &m_mat->roughness, 0.01f, 0.0f, 1.0f);
                    ImGui::DragFloat("Ambient occlusion", &m_mat->ao, 0.01f, 0.0f, 1.0f);
            }
            break;
            }
            case 1: case 2:{
                Light* light = &m_selectedEntity->GetLight();
                const char* light_types[] = {"Point light", "Directional light"};    
                static int type_select = m_selectedEntity->GetType()-1;
                last_light_type = type_select;
                ImGui::Combo("Type", &type_select, light_types, 2);

                if((type_select != last_light_type)){
                    switch(type_select){
                        case 0:
                            light->range = 1.0f;
                            m_selectedEntity->SetType(1);
                            break;
                        case 1:
                            light->range = -1.0f;
                            m_selectedEntity->SetType(2);
                            break;
                    }
                }

                ImGui::ColorEdit3("Color", glm::value_ptr(light->color));
                if(type_select == 0){
                    ImGui::DragFloat("Range", &light->range, 0.1f, 0.0f, FLT_MAX);
                }
                else{
                    light->range = -1.0f;
                }
                ImGui::DragFloat("Intensity", &light->intensity, 0.1f, 0.0f, FLT_MAX);
                break;
            }
        }
    }
    ImGui::End();
}

void Gui::ToolsPanel(){
    ImGui::Begin("Tools");
    if(!ImGui::IsWindowFocused(ImGuiFocusedFlags_AnyWindow)){
        Logic::is_engine_input = true;
    }
    else{
        Logic::is_engine_input = false;
    }
    
    if(ImGui::Button("Play")){
        Logic::is_game = !Logic::is_game; 
    }

    ImGui::End(); 
}

void Gui::AssetViewer(){
    ImGui::Begin("Assets");

    if(ImGui::BeginTabBar("AssetsTabBar", ImGuiTabBarFlags_None)){
        if(ImGui::BeginTabItem("Meshes")){
            for(size_t i = 0; i < meshManager->GetMeshes().size(); i++){
                ImGui::TreeNodeEx((void*)(intptr_t)i, ImGuiTreeNodeFlags_Leaf|ImGuiTreeNodeFlags_NoTreePushOnOpen, meshManager->GetMeshes()[i].GetName().c_str()); 
                if(ImGui::BeginPopupContextItem()){
                    if(ImGui::Button("Add to scene")){
                        Entity entity(&meshManager->GetMeshes()[i], imageManager);
                        entityManager->AddEntity(entity);
                    }
                    ImGui::EndPopup();
                }
            }
            ImGui::EndTabItem();
        }
        if(ImGui::BeginTabItem("Images")){
            for(size_t i = 0; i < imageManager->GetImages().size(); i++){
                ImGui::TreeNodeEx((void*)(intptr_t)i, ImGuiTreeNodeFlags_Leaf|ImGuiTreeNodeFlags_NoTreePushOnOpen, std::to_string(imageManager->GetImages()[i].GetIndex()).c_str());
                if(ImGui::BeginDragDropSource()){
                    size_t imgIndex = imageManager->GetImages()[i].GetIndex();
                    ImGui::SetDragDropPayload("img_to_mat", &imgIndex, sizeof(size_t));
                    ImGui::Text("Drag image to material.");
                    ImGui::EndDragDropSource();
                }
            }
            ImGui::EndTabItem();
        }
        ImGui::EndTabBar();
    }
    ImGui::End();
}

void Gui::MainMenu(){
    if(ImGui::BeginMainMenuBar()){
        if(ImGui::BeginMenu("File")){
            if(ImGui::MenuItem("Save")){
                sceneLoader->SaveScene("scene.json");
            }
            if(ImGui::MenuItem("Load")){
                sceneLoader->LoadScene("scene.json");
            }
            ImGui::EndMenu();
        }
        if(ImGui::BeginMenu("Import")){
            if(ImGui::MenuItem("Mesh")){
                ImGuiFileDialog::Instance()->OpenDialog("ImportMeshFileDialog", "Choose File", ".obj\0.fbx\0.dae\0\0", ".");
            }
            if(ImGui::MenuItem("Image")){
                ImGuiFileDialog::Instance()->OpenDialog("ImportImageFileDialog", "Choose File", ".jpg\0.png\0.bmp\0\0", ".");
            }
            ImGui::EndMenu();
        }
        if(ImGui::BeginMenu("Objects")){
            if(ImGui::MenuItem("Point light")){
                Entity tmpEntity;
                tmpEntity.SetImageManager(imageManager);
                tmpEntity.SetType(1); // 1 - point light
                entityManager->AddEntity(tmpEntity);
            }
            if(ImGui::MenuItem("Directional light")){
                Entity tmpEntity;
                tmpEntity.SetImageManager(imageManager);
                tmpEntity.SetType(2); // 1 - directional light
                entityManager->AddEntity(tmpEntity);
            }
            ImGui::EndMenu();
        }
        ImGui::EndMainMenuBar();
    }
}
