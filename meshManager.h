#ifndef MESH_MANAGER_H
#define MESH_MANAGER_H

#include "mesh.h"

class MeshManager{
public:
    MeshManager();
    ~MeshManager();

    void AddMesh(std::string path);
    void Reset();
    std::vector<Mesh>& GetMeshes();
private:
    bool ImportMesh(std::string filename);
    void RecursiveProcess(aiNode* node, const aiScene* scene, bool& first);
    void ProcessMesh(aiMesh* mesh, const aiScene* scene, bool& first);
    Mesh* lastMesh;
    std::vector<Mesh> m_meshes;

    size_t numChilds;
    std::string m_path;
    std::string m_name;
};

#endif
