#include "sceneLoader.h"
#include "gui.h"

SceneLoader::SceneLoader(){

}

SceneLoader::SceneLoader(ImageManager* imageManager, MeshManager* meshManager, EntityManager* entityManager, Gui* gui){
    this->imageManager = imageManager;
    this->meshManager = meshManager;
    this->entityManager = entityManager;
    this->gui = gui;
}

SceneLoader::~SceneLoader(){

}

void SceneLoader::json_to_EI(const json& j, EntityInfo& ei){
    j.at("name").get_to(ei.name);
    j.at("type").get_to(ei.type);
    j.at("mesh").get_to(ei.mesh); 

    float position[3];
    j.at("position").get_to(position);
    ei.position[0] = position[0];
    ei.position[1] = position[1];
    ei.position[2] = position[2];

    float rotation[3];
    j.at("rotation").get_to(rotation);
    ei.rotation[0] = rotation[0];
    ei.rotation[1] = rotation[1];
    ei.rotation[2] = rotation[2];

    float scale[3];
    j.at("scale").get_to(scale);
    ei.scale[0] = scale[0];
    ei.scale[1] = scale[1];
    ei.scale[2] = scale[2];
    
    // material 
    float matAlbedoCol[3];
    json j_mat = j["material"];
    j_mat.at("albedo_col").get_to(matAlbedoCol);
    ei.mat.albedo_col = glm::make_vec3(matAlbedoCol);
    
    j_mat.at("albedo_tex").get_to(ei.mat.albedo_tex);
    j_mat.at("metallic").get_to(ei.mat.metallic);
    j_mat.at("roughness").get_to(ei.mat.roughness);
    j_mat.at("ao").get_to(ei.mat.ao);
   
    // light
    if(ei.type != 0){ 
        float light_col[3];
        float light_pos[3];
        json j_light = j["light"];
        j_light.at("color").get_to(light_col);
        ei.light.color = glm::make_vec3(light_col);
        
        j_light.at("position").get_to(light_pos);
        ei.light.position = glm::make_vec3(light_pos);
        
        j_light.at("intensity").get_to(ei.light.intensity);
        j_light.at("range").get_to(ei.light.range);
    }
   
    json j_childs = j["childs"];
    for(json::iterator it = j_childs.begin(); it != j_childs.end(); ++it){
        EntityInfo tmp_ei;
        json_to_EI(it.value(), tmp_ei);
        ei.childs.push_back(tmp_ei);
    }
}

void SceneLoader::Mat_to_json(json& j, const sl::Material& mat){
    float albedo_col[3] = {
        mat.albedo_col.r,
        mat.albedo_col.g,
        mat.albedo_col.b
    };

    j = json{
        {"albedo_col", albedo_col},
        {"albedo_tex", mat.albedo_tex},
        {"metallic", mat.metallic},
        {"roughness", mat.roughness},
        {"ao", mat.ao}
    };
}

void SceneLoader::Light_to_json(json& j, const Light& light){
    float col[3] = {
        light.color.r,
        light.color.g,
        light.color.b
    };

    float pos[3] = {
        light.position.x,
        light.position.y,
        light.position.z
    };
    
    j = json{
        {"position", pos},
        {"color", col},
        {"range", light.range},
        {"intensity", light.intensity}
    };
}

// EntityInfo to json
void SceneLoader::EI_to_json(json& j, const EntityInfo& ei){
    json mat, light;
    Mat_to_json(mat, ei.mat);
    Light_to_json(light, ei.light);
    j = {
        {"type", ei.type},
        {"mesh", ei.mesh},
        {"light", light},
        {"name", ei.name},
        {"position", ei.position},
        {"rotation", ei.rotation},
        {"scale", ei.scale},
        {"material", mat},
    };

    std::vector<json> j_childs;
    for(auto& i : ei.childs){
        json tmp_child;
        EI_to_json(tmp_child, i);
        j_childs.push_back(tmp_child);
    }
    j["childs"] = j_childs;
}

bool SceneLoader::LoadScene(const char* filename)
{
    std::ifstream in(filename);
    if(!in.is_open()){
        std::cout << "Failed to load a scene " << filename << std::endl;
        return false;
    }

    meshManager->Reset();
    entityManager->Reset();
    imageManager->Reset();

    json j;
    in >> j;

    json j_imgs = j["images"];
    for (json::iterator it = j_imgs.begin(); it != j_imgs.end(); ++it) {
        std::string path = it.value();
        imageManager->AddImage(path.c_str());
    }

    json j_meshes = j["meshes"];
    
    for(json::iterator it = j_meshes.begin(); it != j_meshes.end(); ++it) {
        std::string path = it.value()[1];
        meshManager->AddMesh(path);
    }

    json j_entities = j["entities"]; 
    for(json::iterator it = j_entities.begin(); it != j_entities.end(); ++it){
        EntityInfo ei;
        Entity entity;
        RecursiveEntitiesLoad(it.value(), ei, entity);
        entityManager->AddEntity(entity);
    }
    
    if(entityManager->GetEntities().size() > 0)
        gui->SelectEntity(&entityManager->GetEntities()[0]);

    return true;
}

void SceneLoader::RecursiveEntitiesSave(EntityInfo& ei, Entity& entity){
    ei.type = entity.GetType();

    if(ei.type == 0)
        ei.mesh = entity.GetMesh()->GetIndex();
    else
        ei.mesh = -1;
    ei.light = entity.GetLight();
    ei.name = entity.GetName();
    glm::vec3 position = entity.GetPosition();
    glm::vec3 rotation = entity.GetRotation();
    glm::vec3 scale = entity.GetScale();

    ei.position[0] = position.x;
    ei.position[1] = position.y;
    ei.position[2] = position.z;
    
    ei.rotation[0] = rotation.x;
    ei.rotation[1] = rotation.y;
    ei.rotation[2] = rotation.z;
    
    ei.scale[0] = scale.x;
    ei.scale[1] = scale.y;
    ei.scale[2] = scale.z;

    sl::Material material;
    Material& original_material = entity.GetMaterial();
    material.albedo_col = original_material.albedo_col;
    if(original_material.albedo_tex == nullptr)
        material.albedo_tex = 0;
    else
        material.albedo_tex = original_material.albedo_tex->GetIndex();

    material.metallic = original_material.metallic;
    material.roughness = original_material.roughness;
    material.ao = original_material.ao;

    ei.mat = material;
    
    for(auto& i : entity.GetChilds()){
        EntityInfo child_ei;
        RecursiveEntitiesSave(child_ei, i);
        ei.childs.push_back(child_ei);
    }
}

Mesh* SceneLoader::RecursiveMeshesLoad(Mesh* mesh, EntityInfo& ei){
    if(mesh->GetIndex() == ei.mesh){
        return mesh;
    }
    for(auto& i : mesh->GetChildMeshes()){
        Mesh* m = RecursiveMeshesLoad(&i, ei);
        if(m == nullptr)
            continue;
        else
            return m;
    }
}

void SceneLoader::RecursiveEntitiesLoad(json& j, EntityInfo& ei, Entity& entity){
    json_to_EI(j, ei);

    entity.SetImageManager(imageManager);
    if(ei.type == 0){
        for(auto& i : meshManager->GetMeshes()){
            /*if(i.GetIndex() == ei.mesh){
                entity.SetMesh(&i);
            }*/
            Mesh* entityMesh = RecursiveMeshesLoad(&i, ei);
            entity.SetMesh(entityMesh);
        }
    }
   
    Material material;
    material.albedo_col = ei.mat.albedo_col;
    material.metallic = ei.mat.metallic;
    material.roughness = ei.mat.roughness;
    material.ao = ei.mat.ao;

    for(auto& i : imageManager->GetImages()){
        if(i.GetIndex() == ei.mat.albedo_tex){
            material.albedo_tex = &i;
            break;
        }
    }

    entity.SetPosition(glm::make_vec3(ei.position));
    entity.SetRotation(glm::make_vec3(ei.rotation));
    entity.SetScale(glm::make_vec3(ei.scale));
    entity.SetName(ei.name);
    entity.SetType(ei.type);
    if(ei.type != 0){
        if(ei.type == 2)
            ei.light.range = -1;   
        entity.SetLight(ei.light);
    }
        
    entity.SetMaterial(material);

    json j_child = j["childs"];
    for(json::iterator it = j_child.begin(); it != j_child.end(); ++it){
        EntityInfo child_ei;
        Entity tmpEntity;
        RecursiveEntitiesLoad(it.value(), child_ei, tmpEntity);
        entity.GetChilds().push_back(tmpEntity);
        ei.childs.push_back(child_ei);
    }
}

bool SceneLoader::SaveScene(const char* filename)
{
    json j;
    std::vector<std::string> imagesPath;
    std::map<size_t, std::string> meshesPath;
    
    for(auto& i : imageManager->GetImages()){
        imagesPath.push_back(i.GetPath());
    }

    j["images"] = imagesPath;
    std::vector<Mesh>& meshes = meshManager->GetMeshes();
    for(auto& i : meshes){
        meshesPath.insert(std::pair<size_t, std::string>(i.GetIndex(), i.GetPath()));
    }
    j["meshes"] = meshesPath;

    std::vector<json> entitiesData;
    std::vector<Entity>& entities = entityManager->GetEntities();

    for(auto& i : entities){
        EntityInfo ei;
        RecursiveEntitiesSave(ei, i);
        
        json tmpJson;
        EI_to_json(tmpJson, ei);
        
        entitiesData.push_back(tmpJson);
    }

    j["entities"] = entitiesData;

    std::ofstream out(filename);
    if(!out.is_open()){
        std::cout << "Failed to write a scene to " << filename << std::endl;
        return false;
    }

    out << std::setw(4) << j << std::endl;

    return true;
}
