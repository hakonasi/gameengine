#version 330 core

uniform sampler2D m_texture;
in vec2 uv;

out vec3 color;

void main(){
    color = texture(m_texture, uv).rgb;
}
