#ifndef SCENE_LOADER
#define SCENE_LOADER

#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>
#include <nlohmann/json.hpp>
#include "imageManager.h"
#include "mesh.h"
#include "entity.h"
#include "meshManager.h"
#include "entityManager.h"
#include "light.h"

using json = nlohmann::json;

namespace sl{
    struct Material{
        glm::vec3 albedo_col = glm::vec3(1.0f, 1.0f, 1.0f);
        size_t    albedo_tex = 0;  

        float metallic = 0.0f;
        float roughness = 0.0f;
        float ao = 0.0f;
    };
};

struct EntityInfo{
    long long mesh;
    Light light;
    size_t image;
    std::string name;
    
    float position[3];
    float rotation[3];
    float scale[3];

    int type;

    sl::Material mat;
    std::vector<EntityInfo> childs;
};

class Gui;

class SceneLoader{
public:
    SceneLoader();
    SceneLoader(ImageManager* imageManager, MeshManager* meshManager, EntityManager* entityManager, Gui* gui);
    ~SceneLoader();

    bool LoadScene(const char* filename);

    bool SaveScene(const char* filename);
private:
    // EntityInfo to json
    void EI_to_json(json& j, const EntityInfo& ei);
    // json to EntityInfo 
    void json_to_EI(const json& j, EntityInfo& ei);

    // Material to json
    void Mat_to_json(json& j, const sl::Material& mat);

    void Light_to_json(json& j, const Light& light);
    
    void RecursiveEntitiesSave(EntityInfo& ei, Entity& entity);
    void RecursiveEntitiesLoad(json& j, EntityInfo& ei, Entity& entity);
    Mesh* RecursiveMeshesLoad(Mesh* mesh, EntityInfo& ei);

    ImageManager* imageManager;
    MeshManager* meshManager;
    EntityManager* entityManager;
    Gui* gui;
};

#endif
