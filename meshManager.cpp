#include "meshManager.h"

MeshManager::MeshManager(){
    // set the meshes limit to 10000
    m_meshes.reserve(10000);
}

MeshManager::~MeshManager(){

}

void MeshManager::AddMesh(std::string path){
    ImportMesh(path);    
}

bool MeshManager::ImportMesh(std::string filename){
    this->m_path = filename;
    bool first = true;
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(filename, aiProcess_Triangulate | aiProcess_GenSmoothNormals);

    if(scene == NULL){
        std::cout << "Failed to load mesh " << filename << ", probably file is missing." << std::endl;
        return false;
    }

    if(scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode || !scene){
        std::cout << importer.GetErrorString() << std::endl;
        return false;
    }
    RecursiveProcess(scene->mRootNode, scene, first);
    return true;
}

void MeshManager::RecursiveProcess(aiNode* node, const aiScene* scene, bool& first){
    for(uint i = 0; i < node->mNumMeshes; i++){
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        numChilds = node->mNumChildren;
        m_name = node->mName.C_Str();
        std::cout << m_name << std::endl;
        ProcessMesh(mesh, scene, first);
        first = false;
    }

    for(uint i = 0; i < node->mNumChildren; i++){
        RecursiveProcess(node->mChildren[i], scene, first);
    }
}

void MeshManager::ProcessMesh(aiMesh* mesh, const aiScene* scene, bool& first){
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<uint> indices;
    std::vector<glm::vec2> uvs;

    for(uint i = 0; i < mesh->mNumVertices; i++){
        glm::vec3 tempVec;
        tempVec.x = mesh->mVertices[i].x;
        tempVec.y = mesh->mVertices[i].y;
        tempVec.z = mesh->mVertices[i].z;
        vertices.push_back(tempVec);
   
        tempVec.x = mesh->mNormals[i].x;
        tempVec.y = mesh->mNormals[i].y;
        tempVec.z = mesh->mNormals[i].z;
        normals.push_back(tempVec);

        glm::vec2 tempUV;
        if(mesh->mTextureCoords[0]){
            tempUV.x = mesh->mTextureCoords[0][i].x;
            tempUV.y = mesh->mTextureCoords[0][i].y;
        }
        else{
            tempUV.x = 0;
            tempUV.y = 0;
        }
        uvs.push_back(tempUV);
    }
    for(uint i = 0; i < mesh->mNumFaces; i++){
        aiFace face = mesh->mFaces[i];
        indices.push_back(face.mIndices[0]);
        indices.push_back(face.mIndices[1]);
        indices.push_back(face.mIndices[2]);
    }

    if(first){
        m_meshes.push_back(Mesh(vertices, normals, indices, uvs));
        std::cout << "meshes size: " << m_meshes.size() << std::endl;    
        lastMesh = &m_meshes.at(m_meshes.size()-1);
        lastMesh->SetPath(m_path);
        lastMesh->SetName(m_name);
    }
    else{
        std::vector<Mesh>& childMeshes = lastMesh->GetChildMeshes();

        childMeshes.push_back(Mesh(vertices, normals, indices, uvs));
        childMeshes.at(childMeshes.size()-1).SetName(m_name);
        if(numChilds > 0){
            lastMesh = &childMeshes.at(lastMesh->GetChildMeshes().size()-1);
            lastMesh->SetName(m_name);
            std::cout << "last mesh name: " << m_name << std::endl;
        }
    }
}

void MeshManager::Reset(){
    lastMesh = nullptr;
    for(auto& i : m_meshes){
        i.Clean();
    }
    m_meshes.clear();
    numChilds = 0;
    m_path.clear();
    m_name.clear();
    Mesh::indicesCounter = 0;
}

std::vector<Mesh>& MeshManager::GetMeshes(){
    return m_meshes;
}
