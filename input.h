#ifndef INPUT_H
#define INPUT_H

#include <vector>
#include <tuple>
#include <functional>
#include "window.h"
#include "logic.h"
#include "camera.h"

class Camera;

class Input{
public:
    Input();
    Input(GLFWwindow* window, Camera* camera);
    ~Input();
    
    bool IsKeyPressed(int key);
    bool IsMousePressed(int key);
    void AddKeyEvent(int key, int action, std::function<void()> func); 
    // обновить mouse delta
    void Update();
    void EngineInput();
    float GetDeltaX();
    float GetDeltaY();
private:
    GLFWwindow* window;
    Camera* camera;
    static void InputCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
    void KeyCallback(int key, int scancode, int action, int mods);
    // Очередь на обработку нажатий клавиш
    // int - клавиша, void* - функция, которая выполняется
    // при нажатии на клавишу
    std::vector< std::tuple<int, int, std::function<void()> >> keyEvents;
    float deltaX = 0;
    float deltaY = 0;
    double lastX, lastY;
};

#endif
