#include "entity.h"

size_t Entity::s_indicesCounter = 0;

Entity::Entity(){
    position = g_position = glm::vec3(0.0f, 0.0f, 0.0f);
    rotation = g_rotation = glm::vec3(0.0f, 0.0f, 0.0f);
    scale = g_scale = glm::vec3(1.0f, 1.0f, 1.0f);
    m_index = s_indicesCounter;
    s_indicesCounter++;

    m_parent = nullptr;
    model = glm::mat4(1.0f);    
    type = 0;
}

Entity::Entity(Mesh* mesh, ImageManager* imageManager){
    this->mesh = mesh;
    this->imageManager = imageManager;
    position = g_position = glm::vec3(0.0f, 0.0f, 0.0f);
    rotation = g_rotation = glm::vec3(0.0f, 0.0f, 0.0f);
    scale = g_scale = glm::vec3(1.0f, 1.0f, 1.0f);
    
    model = glm::mat4(1.0f);    

    m_name = mesh->GetName();
    m_index = s_indicesCounter;
    s_indicesCounter++;

    m_parent = nullptr;
    type = 0;
    
    for(auto& i : mesh->GetChildMeshes()){
        childs.push_back(Entity(&i, imageManager));
        childs[childs.size()-1].SetParent(this);
    }
}

Entity::~Entity(){

}

std::string& Entity::GetName(){
    return m_name;
}

void Entity::SetType(int type){
    int last_type = this->type;
    this->type = type;

    // directional light
    if(type == 2)
        light.range = -1;

    if(last_type == 0){
        switch(type){
            case 1:
                m_name = "Point light";
                gizmo = Gizmo(imageManager, 0);
                break;
            case 2:
                m_name = "Directional light";
                gizmo = Gizmo(imageManager, 0);
                break;    
        }
    }
}

void Entity::SetImageManager(ImageManager* imageManager){
    this->imageManager = imageManager;
}

int Entity::GetType(){
    return type;
}

void Entity::SetLight(Light& light){
    this->light = light;
}

Light& Entity::GetLight(){
    return light;
}

void Entity::SetName(std::string name){
    this->m_name = name;
}

void Entity::SetMaterial(Material material){
    this->material = material;
}

Material& Entity::GetMaterial(){
    return material;
}

std::vector<Entity>& Entity::GetChilds(){
    return childs;
}

size_t Entity::GetIndex(){
    return m_index;
}

void Entity::SetPosition(glm::vec3 position){
    this->position = position;
}

void Entity::SetRotation(glm::vec3 rotation){
    this->rotation = rotation;
}

void Entity::SetScale(glm::vec3 scale){
    this->scale = scale;
}

void Entity::SetMesh(Mesh* mesh){
    this->mesh = mesh;
}

glm::vec3& Entity::GetPosition(){
    return position;
}

glm::vec3& Entity::GetRotation(){
    return rotation;
}

glm::vec3& Entity::GetScale(){
    return scale;
}

Mesh* Entity::GetMesh(){
    return mesh;
}

void Entity::SetParent(Entity* parentEntity){
    this->m_parent = parentEntity;
}

void Entity::Render(Shader& shader, Shader& gizmo_shader, Camera* camera){
    shader.Use();
    if(m_parent != nullptr){
        g_position = position + m_parent->GetPosition();
        g_rotation = rotation + m_parent->GetRotation();
        g_scale    = scale * m_parent->GetScale();
    }
    else{
        g_position = position;
        g_rotation = rotation;
        g_scale    = scale;
    }
    
    glUniform3fv(shader.GetID("camPos"), 1, glm::value_ptr(camera->GetPosition()));
    
    switch(type){
        case 0:{
            model = glm::translate(glm::mat4(1.0f), g_position)
                * glm::rotate(glm::mat4(1.0f), glm::radians(g_rotation.x), glm::vec3(1,0,0))
                * glm::rotate(glm::mat4(1.0f), glm::radians(g_rotation.y), glm::vec3(0,1,0))
                * glm::rotate(glm::mat4(1.0f), glm::radians(g_rotation.z), glm::vec3(0,0,1))
                * glm::scale(glm::mat4(1.0f), g_scale);

            glm::mat4 mvp = camera->GetProjection() * camera->GetView() * model;
            glUniformMatrix4fv(shader.GetID("mvp"), 1, GL_FALSE, glm::value_ptr(mvp));
            glUniformMatrix4fv(shader.GetID("modelMatrix"), 1, GL_FALSE, glm::value_ptr(model));
           
            glUniform3fv(shader.GetID("material.albedo_col"), 1, glm::value_ptr(material.albedo_col));
            glUniform1fv(shader.GetID("material.metallic"), 1, &material.metallic);
            glUniform1fv(shader.GetID("material.roughness"), 1, &material.roughness);
            glUniform1fv(shader.GetID("material.ao"), 1, &material.ao);
            if(material.albedo_tex != nullptr){
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, material.albedo_tex->GetTexture());
            }
            mesh->Render();

            for(auto& i : childs){
                i.Render(shader, gizmo_shader, camera);
            }      
            break;
        }
        case 1:{
            light.position = g_position; 
            glUniform3fv(shader.GetID("light.position"), 1, glm::value_ptr(light.position));
            glUniform3fv(shader.GetID("light.color"), 1, glm::value_ptr(light.color));
            glUniform1fv(shader.GetID("light.range"), 1, &light.range);
            glUniform1fv(shader.GetID("light.intensity"), 1, &light.intensity);

            gizmo.SetPosition(g_position);
            gizmo.Render(gizmo_shader, camera); 
            break;
        }
        case 2:{
            light.position = g_rotation; 
            glUniform3fv(shader.GetID("light.position"), 1, glm::value_ptr(light.position));
            glUniform3fv(shader.GetID("light.color"), 1, glm::value_ptr(light.color));
            glUniform1fv(shader.GetID("light.range"), 1, &light.range);
            glUniform1fv(shader.GetID("light.intensity"), 1, &light.intensity);

            gizmo.SetPosition(g_position);
            gizmo.Render(gizmo_shader, camera); 
            break;
        }
    }
}
