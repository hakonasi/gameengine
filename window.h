#ifndef WINDOW_H
#define WINDOW_H

#include <iostream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "input.h"
#include "camera.h"

typedef unsigned int uint;

class Input;
class Camera;

class Window{
    public:
        Window(int width, int height, const char* title, uint glMajor, uint glMinor);
        ~Window();

        bool Init();
        void Update( void (*rendering)() );
        void Exit();
        GLFWwindow* GetWindow();
    
        void SetInput(Input* input);
        void SetCamera(Camera* camera);
        Input* GetInput();
        int GetWidth();
        int GetHeight();
        void SetSize(int width, int height);
    private:
        int m_width;
        int m_height;
        
        uint m_glMajor;
        uint m_glMinor;

        // NS = non static
        void ResizeCallbackNS(int width, int height);
        static void ResizeCallback(GLFWwindow* window, int width, int height);
        const char* title;
        Input* m_input;
        GLFWwindow* m_window;
        Camera* camera;
};

#endif
