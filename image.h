#ifndef IMAGE_H
#define IMAGE_H

#include "window.h"
#include <SOIL.h>

class Image{
public:
    Image();
    Image(const char* filename);

    void LoadImage(const char* filename);

    size_t GetIndex();

    std::string GetPath();
    GLuint GetTexture();
   
    int GetWidth();
    int GetHeight();

    static size_t indicesCounter;
private:
    GLuint CreateTexture(int width, int height, unsigned char* data);
    std::string path;
    size_t index;
    GLuint m_texture;
    
    int m_width;
    int m_height;
};

#endif
