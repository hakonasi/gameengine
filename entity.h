#ifndef ENTITY_H
#define ENTITY_H

#include "mesh.h"
#include "imageManager.h"
#include "material.h"
#include "light.h"
#include "gizmo.h"

class Entity{
    public:
        Entity();
        Entity(Mesh* mesh, ImageManager* imageManager);
        ~Entity();

        void SetImageManager(ImageManager* imageManager);

        void SetLight(Light& light);
        Light& GetLight();

        void SetMesh(Mesh* mesh);
        Mesh* GetMesh();

        void SetType(int type);
        int GetType();

        void SetParent(Entity* parentEntity);

        void SetMaterial(Material material);
        Material& GetMaterial();        

        std::vector<Entity>& GetChilds();
        std::string& GetName();

        glm::vec3& GetPosition();
        glm::vec3& GetRotation();
        glm::vec3& GetScale();

        size_t GetIndex();
        void SetName(std::string name);
        void SetPosition(glm::vec3 position);
        void SetRotation(glm::vec3 rotation);
        void SetScale(glm::vec3 scale);
        void Render(Shader& shader, Shader& gizmo_shader, Camera* camera);
        
        static size_t s_indicesCounter;
    private:
        std::vector<Entity> childs;
        Mesh* mesh;
        ImageManager* imageManager;

        Light light;
        Gizmo gizmo;

        glm::vec3 position;
        glm::vec3 rotation;
        glm::vec3 scale;

        glm::vec3 g_position;
        glm::vec3 g_rotation;
        glm::vec3 g_scale;

        glm::mat4 model;

        // 0 - mesh
        // 1 - light
        int type = 0;

        size_t m_index;
        Material material;

        std::string m_name;
        Entity* m_parent;
};

#endif
