#ifndef MESH_H
#define MESH_H

#include "shader.h"
#include <vector>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

class Mesh{
public:
    Mesh();
    Mesh(std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& normals, std::vector<uint>& indices, std::vector<glm::vec2>& uvs);
    ~Mesh();

    void Render();
    void Clean();
    
    void SetName(std::string name);
    std::string GetName();

    void SetPath(std::string path);
    std::string GetPath();
    std::vector<Mesh>& GetChildMeshes();

    size_t GetIndex();
    static size_t indicesCounter;
private:
    void Init(std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& normals, std::vector<uint>& indices, std::vector<glm::vec2>& uvs);

    uint vao;
    uint buffers[3];
    uint idx_buf;

    uint indices_size;

    size_t index;

    std::string name;

    std::string path;    
    uint numChilds;
    std::vector<Mesh> childMeshes;
};

#endif
